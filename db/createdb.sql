CREATE DATABASE IF NOT EXISTS `connectingamers`;

USE `connectingamers`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(150) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `name` VARCHAR(150),
  `surname` VARCHAR(150),
  `birthday` DATETIME,
  `signup_date` DATETIME NOT NULL,
  `role` ENUM('user', 'admin') NOT NULL,
  `image` VARCHAR(150),

   PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `users_comments` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_emisor` INTEGER NOT NULL,
  `id_receptor` INTEGER NOT NULL,
  `message` VARCHAR(1500) NOT NULL,
  `fecha` DATETIME NOT NULL,

   PRIMARY KEY (`id`),

   CONSTRAINT fk_users_comments_id_users_emisor FOREIGN KEY (id_emisor) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_users_comments_id_users_receptor FOREIGN KEY (id_receptor) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `conversaciones` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_one` INTEGER NOT NULL,
  `user_two` INTEGER NOT NULL,

   PRIMARY KEY (`id`),
   UNIQUE KEY `id_user_game` (`user_one`, `user_two`),
   CONSTRAINT fk_conversaciones_id_user_one FOREIGN KEY (user_one) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_conversaciones_id_user_two FOREIGN KEY (user_two) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `conversacion_messages` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_user` INTEGER NOT NULL,
  `id_conversacion` INTEGER NOT NULL,
  `message` VARCHAR(1500) NOT NULL,
  `fecha` DATETIME NOT NULL,

   PRIMARY KEY (`id`),

   CONSTRAINT fk_conversacion_messages_id_user FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_conversacion_messages_id_conversacion FOREIGN KEY (id_conversacion) REFERENCES conversaciones(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `games` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `imagen` varchar(50),
  `puntuacion` FLOAT DEFAULT 0,
  `genero` ENUM('Lucha', 'Acción', 'Rol', 'Acción RPG', 'Conducción', 'Aventura', 'Plataformas', 'Minijuegos', 'Hack and Slash'),
  `plataforma` ENUM('PS4', 'XONE', 'PC', 'Wii U'),
  `descripcion` varchar(1000) NOT NULL,

   PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `users_games` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_user` INTEGER,
  `id_game` INTEGER,
  `puntuacion` INTEGER NOT NULL DEFAULT 0,
  `estado` ENUM('Jugado', 'Jugando', 'Pendiente', 'No interesa'),

   PRIMARY KEY (`id`),
   UNIQUE KEY `id_user_game` (`id_user`, `id_game`),
   CONSTRAINT fk_usersgames_id_users FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_usersgames_id_games FOREIGN KEY (id_game) REFERENCES games(id) ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS `users_follow` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_user_seguido` INTEGER,
  `id_user_seguidor` INTEGER,

   PRIMARY KEY (`id`),
   UNIQUE KEY `id_user_follow` (`id_user_seguido`, `id_user_seguidor`),
   CONSTRAINT fk_usersfollow_id_user_seguido FOREIGN KEY (id_user_seguido) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_usersfollow_id_user_seguidor FOREIGN KEY (id_user_seguidor) REFERENCES users(id) ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS `users_block` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_user_bloqueador` INTEGER,
  `id_user_bloqueado` INTEGER,

   PRIMARY KEY (`id`),
   UNIQUE KEY `id_user_block` (`id_user_bloqueador`, `id_user_bloqueado`),
   CONSTRAINT fk_usersfollow_id_user_bloqueador FOREIGN KEY (id_user_bloqueador) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_usersfollow_id_user_bloqueado FOREIGN KEY (id_user_bloqueado) REFERENCES users(id) ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS `news` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `id_user` INTEGER,
  `date` DATETIME NOT NULL,

   PRIMARY KEY (`id`),
   CONSTRAINT fk_news_id_users FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `users_critics` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_emisor` INTEGER NOT NULL,
  `id_juego` INTEGER NOT NULL,
  `message` VARCHAR(1500) NOT NULL,
  `fecha` DATETIME NOT NULL,

   PRIMARY KEY (`id`),

   CONSTRAINT fk_users_critics_id_users_emisor FOREIGN KEY (id_emisor) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_users_critics_id_game_receptor FOREIGN KEY (id_juego) REFERENCES games(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `users_opiniones` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `id_emisor` INTEGER NOT NULL,
  `id_noticia` INTEGER NOT NULL, 
  `message` VARCHAR(1500) NOT NULL,
  `fecha` DATETIME NOT NULL,

   PRIMARY KEY (`id`),

   CONSTRAINT fk_users_opiniones_id_users_emisor FOREIGN KEY (id_emisor) REFERENCES users(id) ON DELETE CASCADE,
   CONSTRAINT fk_users_opiniones_id_noticia_receptora FOREIGN KEY (id_noticia) REFERENCES news(id) ON DELETE CASCADE
);