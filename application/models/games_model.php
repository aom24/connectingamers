<?php

class games_model extends CI_Model {
	private $tabla = 'games';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/
	
	public function insert($game) 
	{
		$item = $this->creaItem($game);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($juego) 
	{
		$item = $this->creaItem($juego);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAllWithLimit($limit) 
	{
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	public function readByGenero($id, $genero, $limit) 
	{
		$this->db->where('genero', $genero);
		$this->db->where('id !=', $id);
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	public function readAllWithLimitOrderByCal($limit) 
	{
		$this->db->order_by('puntuacion', 'desc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	public function readGame($titulo) 
	{
		$this->db->order_by('titulo', 'asc');
		$this->db->like('titulo', $titulo);
		return $this->db->get($this->tabla)->result();
	}

	public function updatePuntuacion($juego) 
	{
		$item = $this->creaItemUpdatePuntuacion($juego);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

    function filas() 
    {
        $consulta = $this->db->get($this->tabla);
        return  $consulta->num_rows() ;
    }
        
    function total_paginados($por_pagina,$segmento) 
    {
    	$consulta = $this->db->get($this->tabla,$por_pagina,$segmento);
        if($consulta->num_rows()>0) {
        	foreach($consulta->result() as $fila) {
            	$data[] = $fila;
        	}
           	return $data;
    	}
    }

    public function total_filtrados($plataformas, $texto) 
    {
        $this->db->where_in('plataforma', $plataformas);
        if(!empty($texto))
       		$this->db->like('titulo',$texto);
       	$this->db->order_by($this->tabla . '.id', 'asc');
        return $this->db->get($this->tabla)->result();     
    }

	private function creaItem($game) 
	{
		return $item = array("id" => $game['id'], "titulo" => $game['titulo'], "puntuacion" => $game['puntuacion'], 
						     "imagen" => $game['imagen'], "descripcion" => $game['descripcion'], "plataforma" => $game['plataforma'],
						     "genero" => $game['genero']
					    	);
	}

	private function creaItemUpdatePuntuacion($game) 
	{
		return $item = array("id" => $game['id'], "puntuacion" => $game['puntuacion']);
	}
}

?>