<?php

class users_comments_model extends CI_Model {
	private $tabla = 'users_comments';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/

	public function insert($user_comment) 
	{
		$item = $this->creaItem($user_comment);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}
	
	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($user_comment) 
	{
		$item = $this->creaItem($user_comment);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readByReceptor($id_receptor) 
	{ 
		$this->db->where('id_receptor', $id_receptor);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAll() 
	{
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->tabla)->result();
	}

	public function filas($id_receptor) 
	{
    	$this->db->where('id_receptor', $id_receptor);
		$this->db->order_by('fecha', 'desc');
        return  $this->db->get($this->tabla)->num_rows() ;
    }
        
    public function total_paginados($por_pagina,$segmento,$id_receptor) 
    {
        $this->db->where('id_receptor', $id_receptor);
		$this->db->order_by('fecha', 'desc');
        return $this->db->get($this->tabla,$por_pagina,$segmento)->result();     
    }

	private function creaItem($user_comment) 
	{
		return $item = array("id" => $user_comment['id'], "id_emisor" => $user_comment['id_emisor'], "id_receptor" =>$user_comment['id_receptor'], 
			                 "message" =>$user_comment['message'], "fecha" =>$user_comment['fecha']
			                );
	}
}

?>