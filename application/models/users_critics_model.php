<?php

class users_critics_model extends CI_Model {
	private $tabla = 'users_critics';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/

	public function insert($user_critic) 
	{
		$item = $this->creaItem($user_critic);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($user_critic) 
	{
		$item = $this->creaItem($user_critic);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}	

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readByGame($id_game) 
	{
		$this->db->where('id_juego', $id_game);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAll() 
	{
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->tabla)->result();
	}

	public function readLastWithLimit($limit) 
	{
		$this->db->order_by('fecha', 'desc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	public function filas($id_game) 
	{
    	$this->db->where('id_juego', $id_game);
		$this->db->order_by('fecha', 'desc');
        return  $this->db->get($this->tabla)->num_rows() ;
    }
        
    public function total_paginados($por_pagina,$segmento,$id_game) 
    {
        $this->db->where('id_juego', $id_game);
		$this->db->order_by('fecha', 'desc');
        return $this->db->get($this->tabla,$por_pagina,$segmento)->result();     
    }

	private function creaItem($user_critic) 
	{
		return $item = array("id" => $user_critic['id'], "id_emisor" => $user_critic['id_emisor'], "id_juego" =>$user_critic['id_juego'], 
			                 "message" =>$user_critic['message'], "fecha" =>$user_critic['fecha']
			                );
	}
}

?>