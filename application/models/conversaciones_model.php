<?php

class conversaciones_model extends CI_Model {
	private $tabla = 'conversaciones';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/
	
	public function insert($conversacion) 
	{
		$item = $this->creaItem($conversacion);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($conversacion) 
	{
		$item = $this->creaItem($conversacion);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAllbyUser($id_user) 
	{
		$this->db->where('user_one', $id_user);
		$this->db->or_where('user_two', $id_user);
		return $this->db->get($this->tabla)->result();
	}

	public function readbyUsers($user_one, $user_two) 
	{
		$users = array($user_one, $user_two);
		$this->db->where_in('user_one', $users);
		$this->db->where_in('user_two', $users);
		return $this->db->get($this->tabla)->row();
	}

	private function creaItem($conversacion) 
	{
		return $item = array("id" => $conversacion['id'], "user_one" => $conversacion['user_one'], "user_two" => $conversacion['user_two']);
	}
}

?>