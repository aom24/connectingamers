<?php

class games_user_model extends CI_Model {
	private $tabla = 'users_games';
	private $tabla_aux = 'games';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/
	
	public function insert($gameuser) 
	{
		$item = $this->creaItem($gameuser);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($gameuser) 
	{
		$item = $this->creaItem($gameuser);
		$this->db->where('id_game', $item['id_game']);
		$this->db->where('id_user', $item['id_user']);
		$this->db->update($this->tabla, $item);	
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		return $this->db->get($this->tabla)->result();
	}

	public function readByGameUser($id_user, $id_game) 
	{
		$this->db->where('id_user', $id_user);
		$this->db->where('id_game', $id_game);
		return $this->db->get($this->tabla)->row();
	}

	public function readAllGamesByUser($id_user) 
	{
		$this->db->where('id_user', $id_user);
		return $this->db->get($this->tabla)->result();
	}

	public function readAllUsersByGame($id_game) 
	{
		$this->db->where('id_game', $id_game);
		return $this->db->get($this->tabla)->result();
	}

	public function readAllUsersByGameAndEstado($id_game, $estado) 
	{
		$this->db->where('id_game', $id_game);
		$this->db->where('estado', $estado);
		return $this->db->get($this->tabla)->result();
	}

    public function filas($id_user) 
	{
		$this->db->join($this->tabla_aux, $this->tabla . '.id_game = ' . $this->tabla_aux . '.id');
    	$this->db->where('id_user', $id_user);
    	$this->db->order_by($this->tabla . '.id', 'asc');
        return  $this->db->get($this->tabla)->num_rows() ;
    }
        
    public function total_paginados($por_pagina,$segmento,$id_user) 
    {
    	$this->db->join($this->tabla_aux, $this->tabla . '.id_game = ' . $this->tabla_aux . '.id');
        $this->db->where('id_user', $id_user);
        $this->db->order_by($this->tabla . '.id', 'asc');
        return $this->db->get($this->tabla,$por_pagina,$segmento)->result();     
    }

    public function filas_filtradas($id_user, $estados, $plataformas) 
	{
		$this->db->join($this->tabla_aux, $this->tabla . '.id_game = ' . $this->tabla_aux . '.id');
    	$this->db->where('id_user', $id_user);
    	$this->db->where_in('estado', $estados);
    	$this->db->where_in('plataforma', $plataformas);
    	$this->db->order_by($this->tabla . '.id', 'asc');
        return  $this->db->get($this->tabla)->num_rows() ;
    }
        
    public function total_paginados_filtrados($por_pagina,$segmento,$id_user, $estados, $plataformas) 
    {
    	$this->db->join($this->tabla_aux, $this->tabla . '.id_game = ' . $this->tabla_aux . '.id');
        $this->db->where('id_user', $id_user);
        $this->db->where_in('estado', $estados);
        $this->db->where_in('plataforma', $plataformas);
        $this->db->order_by($this->tabla . '.id', 'asc');
        return $this->db->get($this->tabla,$por_pagina,$segmento)->result();     
    }

	public function readAllWithLimit($limit) 
	{
		return $this->db->get($this->tabla, $limit)->result();
	}

	public function updateEstado($gameuser) 
	{
		$item = $this->creaItemUpdateEstado($gameuser);
		$this->db->where('id_game', $item['id_game']);
		$this->db->where('id_user', $item['id_user']);
		$this->db->update($this->tabla, $item);	
	}

	public function updatePuntuacion($gameuser) 
	{
		$item = $this->creaItemUpdatePuntuacion($gameuser);
		$this->db->where('id_game', $item['id_game']);
		$this->db->where('id_user', $item['id_user']);
		$this->db->update($this->tabla, $item);
	}

	public function calificacionMediaGame($id_game) 
	{
		$this->db->where('id_game', $id_game);
		$this->db->where('puntuacion !=', 0);
		$this->db->select_avg('puntuacion');
		return $this->db->get($this->tabla)->row();
	}

	public function totalNumVotos($id_game) 
	{
		$this->db->where('id_game', $id_game);
		$this->db->where('puntuacion !=', 0);
		$this->db->from($this->tabla);
		return $this->db->count_all_results();
	}

	private function creaItem($gameuser) 
	{
		return $item = array("id" => $gameuser['id'], "id_user" => $gameuser['id_user'], "id_game" => $gameuser['id_game'],
						     "puntuacion" => $gameuser['puntuacion'], "estado" => $gameuser['estado'] 
							);
	}

	private function creaItemUpdateEstado($gameuser) 
	{
		return $item = array("id_user" => $gameuser['id_user'], "id_game" => $gameuser['id_game'], "estado" => $gameuser['estado']);
	}

	private function creaItemUpdatePuntuacion($gameuser) 
	{
		return $item = array("id_user" => $gameuser['id_user'], "id_game" => $gameuser['id_game'], "puntuacion" => $gameuser['puntuacion']);
	}
}

?>