<?php

class users_follow_model extends CI_Model {
	private $tabla = 'users_follow';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/
	
	public function insert($follow) 
	{
		$item = $this->creaItem($follow);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($follow) 
	{
		$item = $this->creaItem($follow);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		return $this->db->get($this->tabla)->result();
	}

	public function esSeguidor($id_user_seguido, $id_user_seguidor) 
	{
		$this->db->where('id_user_seguido', $id_user_seguido);
		$this->db->where('id_user_seguidor', $id_user_seguidor);
		return $this->db->get($this->tabla)->row();
	}

	public function sigue($id_user_seguidor, $id_user_seguido) 
	{
		$this->db->where('id_user_seguido', $id_user_seguido);
		$this->db->where('id_user_seguidor', $id_user_seguidor);
		return $this->db->get($this->tabla)->row();
	}

	public function totalSeguidores($id_user) 
	{
		$this->db->where('id_user_seguido', $id_user);
		return $this->db->get($this->tabla)->num_rows();
	}

	public function totalSeguidos($id_user) 
	{ 
		$this->db->where('id_user_seguidor', $id_user);
		return $this->db->get($this->tabla)->num_rows();
	}

	public function readAllSeguidos($id_user) 
	{
		$this->db->where('id_user_seguidor', $id_user);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAllSeguidores($id_user) 
	{
		$this->db->where('id_user_seguido', $id_user);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readSeguidosWithLimit($limit, $id_user) 
	{
		$this->db->where('id_user_seguidor', $id_user);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	public function readSeguidoresWithLimit($limit, $id_user) 
	{
		$this->db->where('id_user_seguido', $id_user);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	private function creaItem($follow) 
	{
		return $item = array("id" => $follow['id'], "id_user_seguido" => $follow['id_user_seguido'], 
			                 "id_user_seguidor" => $follow['id_user_seguidor']
			                );
	}
}

?>