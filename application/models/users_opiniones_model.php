<?php

class users_opiniones_model extends CI_Model {
	private $tabla = 'users_opiniones';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/

	public function insert($user_opinion) 
	{
		$item = $this->creaItem($user_opinion);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($user_opinion) 
	{
		$item = $this->creaItem($user_opinion);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);	
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}
	
/**

Resto de funciones

**/

	public function readByNoticia($id_noticia) 
	{
		$this->db->where('id_noticia', $id_noticia);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function opinionesByNoticia($id_noticia) 
	{
		$this->db->where('id_noticia', $id_noticia);
		$this->db->from($this->tabla);
		return $this->db->count_all_results();
	}

	public function readAll() 
	{
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->tabla)->result();
	}

	public function readLastWithLimit($limit) 
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla, $limit)->result();
	}

	private function creaItem($user_opinion) 
	{
		return $item = array("id" => $user_opinion['id'], "id_emisor" => $user_opinion['id_emisor'], "id_noticia" =>$user_opinion['id_notica'], 
			                 "message" =>$user_opinion['message'], "fecha" =>$user_opinion['fecha']
			                );
	}
}

?>