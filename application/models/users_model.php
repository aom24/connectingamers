<?php

class users_model extends CI_Model {
	private $tabla = 'users';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/

	public function insert($user) 
	{
		$item = $this->creaItem($user);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($user) 
	{
		$item = $this->creaItem($user);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/
	public function buscador($abuscar)
	{
		$this->db->select('id');
		$this->db->select('email');
		$this->db->select('image');
		$this->db->like('email',$abuscar);
		$resultados = $this->db->get($this->tabla, 6);
		if($resultados->num_rows() > 0)
		{
			return $resultados->result();
		}else{
			return FALSE;	
		}	
	}

	public function readByEmail($email) 
	{
		$this->db->where('email', $email);
		return $this->db->get($this->tabla)->row();
	}

	public function readAll() 
	{
		$this->db->order_by('email', 'asc');
		return $this->db->get($this->tabla)->result();
	}

	public function comprueba_login($email, $password) 
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password);

		$usuario = $this->db->get($this->tabla)->row();

		return ($usuario->email == $email);
	}

	public function email_disponible($email) 
	{
		$this->db->where('email', $email);
		$hayEmail = $this->db->get($this->tabla)->row();

		return (!(isset($hayEmail) && !empty($hayEmail)));
	}

	private function creaItem($user)
	{
		return $item = array("id" => $user['id'], "name" => $user['name'], "surname" =>$user['surname'], "birthday" =>$user['birthday'], 
			                 "signup_date" =>$user['signup_date'], "role" =>$user['role'], "email" => $user['email'], 
			                 "password" =>$user['password'], "image" =>$user['image']
			                );
	}
}

?>