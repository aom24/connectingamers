<?php

class conversacion_messages_model extends CI_Model {
	private $tabla = 'conversacion_messages';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/
	
	public function insert($message) 
	{
		$item = $this->creaItem($message);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($message) 
	{
		$item = $this->creaItem($message);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAllbyConversacion($id_conversacion) 
	{
		$this->db->where('id_conversacion', $id_conversacion);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	private function creaItem($message) 
	{
		return $item = array("id" => $message['id'], "id_user" => $message['id_user'], "id_conversacion" => $message['id_conversacion'], 
			 				 "message" => $message['message'], "fecha" => $message['fecha']
							);
	}
}

?>