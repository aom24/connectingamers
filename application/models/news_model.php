<?php

class news_model extends CI_Model {
	private $tabla = 'news';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/

	public function insert($new) 
	{
		$item = $this->creaItem($new);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($new) 
	{
		$item = $this->creaItem($new);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAllbyDate() 
	{
		$this->db->order_by('date', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	public function readAllWithLimit($limit) 
	{
		$this->db->order_by('date', 'desc');
		return $this->db->get($this->tabla, $limit)->result();
	}

    public function filas() 
    {
        $consulta = $this->db->get($this->tabla);
        return  $consulta->num_rows() ;
    }
        
    public function total_paginados($por_pagina,$segmento) 
    {
    	$consulta = $this->db->get($this->tabla,$por_pagina,$segmento);
    	if($consulta->num_rows()>0) {
			foreach($consulta->result() as $fila) {
            	$data[] = $fila;
        	}
 		return $data;
        }
    }

	private function creaItem($new) 
	{
		return $item = array("id" => $new['id'], "title" => $new['title'], "description" => $new['description'], 
						     "id_user" => $new['id_user'], "date" => $new['date']
							);
	}
}

?>