<?php

class users_block_model extends CI_Model {
	private $tabla = 'users_block';

	function __construct() 
	{
		parent::__construct();	
	}

/**

Funciones CRUD

**/
	
	public function insert($block) 
	{
		$item = $this->creaItem($block);
		$this->db->insert($this->tabla, $item);
		$id = $this->db->insert_id();
		return $id;
	}

	public function read($id) 
	{
		$this->db->where('id', $id);
		return $this->db->get($this->tabla)->row();
	}

	public function update($block) 
	{
		$item = $this->creaItem($block);
		$this->db->where('id', $item['id']);
		$this->db->update($this->tabla, $item);
	}

	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tabla);
	}

/**

Resto de funciones

**/

	public function readAll() 
	{
		return $this->db->get($this->tabla)->result();
	}

	public function estaBloqueado($id_user_bloqueado, $id_user_bloqueador) 
	{
		$this->db->where('id_user_bloqueado', $id_user_bloqueado);
		$this->db->where('id_user_bloqueador', $id_user_bloqueador);
		return $this->db->get($this->tabla)->row();
	}

	public function esBloqueador($id_user_bloqueador, $id_user_bloqueado) 
	{
		$this->db->where('id_user_bloqueado', $id_user_bloqueado);
		$this->db->where('id_user_bloqueador', $id_user_bloqueador);
		return $this->db->get($this->tabla)->row();
	}

	public function readAllBloqueados($id_user) 
	{
		$this->db->where('id_user_bloqueador', $id_user);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tabla)->result();
	}

	

	private function creaItem($block) 
	{
		return $item = array("id" => $block['id'], "id_user_bloqueado" => $block['id_user_bloqueado'], 
			                 "id_user_bloqueador" => $block['id_user_bloqueador']
			                );
	}
}

?>