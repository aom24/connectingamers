<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Juego extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('users_model', '', TRUE);
		$this->load->model('games_model', '', TRUE);
		$this->load->model('games_user_model', '', TRUE);
		$this->load->model('users_critics_model', '', TRUE);
		if ( ! $this->session->userdata('email') != '') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	            redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
	    $this->data['games'] = array();
	}

	public function index() 
	{
		$this->load->view('juegos.php', $this->data);
	}

	public function juegos_filtrados() 
	{
		$plataformas = $this->input->post('plataformas');
		$texto = $this->input->post('inputTexto');
       	if($plataformas == '') {
       		$plataformas = null;
       	}
        $juegos = $this->games_model->total_filtrados($plataformas, $texto); 
        foreach($juegos as $juego) {
		 	$game = array(
		 	'id' => $juego->id,
		 	'titulo' => $juego->titulo,
		 	'descripcion' => $juego->descripcion,
		 	'plataforma' => $juego->plataforma,
		 	'genero' => $juego->genero,
		 	'imagen' => $juego->imagen,
        	);
			array_push($this->data['games'], $game);
		}
		$this->load->view('juegos.php', $this->data);
	}

	public function info($id_juego) 
	{
		$this->data['juego'] = $this->games_model->read($id_juego);
		if(!empty($this->data['juego'])) {
			$this->data['listadoJuegos'] = $this->games_model->readByGenero($id_juego, $this->data['juego']->genero, 5);
			$config = $this->config(base_url().'juego/info/'.$id_juego, $this->users_critics_model->filas($id_juego), 5, 3, 4);          
			$criticas = $this->users_critics_model->total_paginados($config['per_page'],$this->uri->segment(4),$id_juego);
			$this->data['criticas'] = array();
			$usuarios = $this->games_user_model->readAllUsersByGame($id_juego);
			$this->data['usuarios'] = array();
			foreach($criticas as $critica) {
				$emisor = $this->users_model->read($critica->id_emisor);
			 	$opinion = array(
			 	'id_critica' => $critica->id,
			 	'id_emisor' => $emisor->id,
			    'email_emisor' => $emisor->email,
			    'image_emisor' => $emisor->image,
		        'message' => $critica->message,
		        'fecha' => $critica->fecha,
	        	);
				array_push($this->data['criticas'], $opinion);
			}
			foreach($usuarios as $usuario) {
				$user = $this->users_model->read($usuario->id_user);
				$usuario_juego = array(
			 	'id' => $user->id,
			 	'user_email' => $user->email,
			    'user_image' => $user->image,
			    'user_estado' => $usuario->estado,
	        	);
				array_push($this->data['usuarios'], $usuario_juego);
			}
			$resultado = $this->games_user_model->readByGameUser($this->data['usuario_conectado']['id'],$id_juego);
			if (empty($resultado)) { 
				$this->data['usuario_juego'] = $usuario_juego = array(
			 	'puntuacion' => 0,
			 	'estado' => 'No interesa',
	        	);
			} else {
				$this->data['usuario_juego'] = array(
			 	'puntuacion' => $resultado->puntuacion,
			 	'estado' => $resultado->estado,
	        	);
			}
			$this->load->view('juego.php', $this->data); 
		} else {
			show_404();
		}
	}

	private function config($base_url, $total_rows, $per_page, $num_links, $uri_segment) 
	{
		$this->load->library('pagination'); //Cargamos la librería de paginación
		$config['base_url'] = $base_url; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	    $config['total_rows'] = $total_rows;//calcula el número de filas  
	    $config['per_page'] = $per_page; //Número de registros mostrados por páginas
	    $config['num_links'] = $num_links; //Número de links mostrados en la paginación
	    $config["uri_segment"] = $uri_segment;//el segmento de la paginación
	    $this->pagination->initialize($config); //inicializamos la paginación
	    return $config;
	}

	public function cambia_estado($id_juego, $estado) 
	{
		$update = array(
				'id_game' => $id_juego,
				'id_user' => $this->data['usuario_conectado']['id'],
				'estado' => $estado,
				'puntuacion' => 0,
		);
		$result = $this->games_user_model->readByGameUser($update['id_user'],$update['id_game']);
		if(!empty($result)) {
			if($update['estado'] == 'Nointeresa') {
				$this->games_user_model->delete($result->id);
				$puntuacion = number_format($this->games_user_model->calificacionMediaGame($id_juego)->puntuacion,1);
				$juego = array(
					'id' => $id_juego,
					'puntuacion' => $puntuacion,
				);
				$this->games_model->updatePuntuacion($juego);
			} else if($update['estado'] == 'Pendiente') {
				$updatePendiente = array(
					'id' => $result->id,
					'id_game' => $id_juego,
					'id_user' => $this->data['usuario_conectado']['id'],
					'estado' => $estado,
					'puntuacion' => 0,
				);
				$this->games_user_model->update($updatePendiente);
				$puntuacion = number_format($this->games_user_model->calificacionMediaGame($id_juego)->puntuacion,1);
				$juego = array(
					'id' => $id_juego,
					'puntuacion' => $puntuacion,
				);
				$this->games_model->updatePuntuacion($juego);
			} else {
				$this->games_user_model->updateEstado($update);
			}
		} else {
			if($update['estado'] != 'Nointeresa') {
				$this->games_user_model->insert($update);
			}
		}
		redirect('juego/' . $id_juego);
	}

	public function cambia_puntuacion($id_juego, $puntuacion) 
	{
		$update = array(
				'id_game' => $id_juego,
				'id_user' => $this->data['usuario_conectado']['id'],
				'estado' => '',
				'puntuacion' => $puntuacion,
		);
		$result = $this->games_user_model->readByGameUser($update['id_user'],$update['id_game']);
		if(!empty($result)) {
			if($puntuacion == '10' || $puntuacion == '8' || $puntuacion == '6' || $puntuacion == '4' || $puntuacion == '2') {
				if($result->estado == 'Pendiente') {
					$this->session->set_flashdata('errorEstado', true);
				} else {
					$this->games_user_model->updatePuntuacion($update);
					$puntuacion = number_format($this->games_user_model->calificacionMediaGame($id_juego)->puntuacion,1);
					$juego = array(
						'id' => $id_juego,
						'puntuacion' => $puntuacion,
					);
					$this->games_model->updatePuntuacion($juego);
				}
			} else {
				$this->session->set_flashdata('errorPuntuacion', true);
			}
		} else {
			$this->session->set_flashdata('errorEstado', true);
		}
		redirect('juego/' . $id_juego);
	}

	public function delete_critica($id_juego, $id_critica) {
		$critica = $this->users_critics_model->read($id_critica);
		if($this->data['usuario_conectado']['id'] == $critica->id_emisor) {
			$this->users_critics_model->delete($id_critica);
			redirect('juego/' . $id_juego);
		} else {
			redirect('user/force_logout');
		}
	}

	public function send_critica($id_juego) {
		$critica = array(
			'id_emisor' => $this->data['usuario_conectado']['id'],
			'id_juego' => $id_juego, 
			'message' => $this->input->post('inputCritica'),
			'fecha' => date('Y-m-d H-i-s'),
		);
		$this->users_critics_model->insert($critica);
		redirect('juego/' . $id_juego);
	}
}