<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('users_model', '', TRUE);
		$this->load->model('users_comments_model', '', TRUE);
		$this->load->model('games_model', '', TRUE);
		$this->load->model('games_user_model', '', TRUE);
		$this->load->model('users_follow_model', '', TRUE);
		$this->load->model('users_block_model', '', TRUE);
		$this->load->model('conversaciones_model', '', TRUE);
		$this->load->model('conversacion_messages_model', '', TRUE);
		$this->load->helper('url');
		if ( ! $this->session->userdata('email') != '') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	            redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
	}

	private function inicializar($id, $pagina) {
		$this->data['usuario'] = $this->users_model->read($id);
		$this->data['total_seguidos'] = $this->users_follow_model->totalSeguidos($id);
		$this->data['total_seguidores'] = $this->users_follow_model->totalSeguidores($id);
		if(!empty($this->users_follow_model->esSeguidor($this->data['usuario_conectado']['id'],$id))) {
			$this->data['esSeguidor'] = true;
		} else {
			$this->data['esSeguidor'] = false;
		}
		if(!empty($this->users_follow_model->sigue($this->data['usuario_conectado']['id'],$id))) {
			$this->data['sigue'] = true;
		} else {
			$this->data['sigue'] = false;
		}
		if(!empty($this->users_block_model->esBloqueador($id,$this->data['usuario_conectado']['id']))) {
			$this->data['esBloqueador'] = true;
		} else {
			$this->data['esBloqueador'] = false;
		}
		if(!empty($this->users_block_model->estaBloqueado($id,$this->data['usuario_conectado']['id']))) {
			$this->data['estaBloqueado'] = true;
		} else {
			$this->data['estaBloqueado'] = false;
		}
		$this->data['menu'] = $pagina;
	}

	public function muro($id) 
	{
		$this->inicializar($id, 'muro');
		if(!empty($this->data['usuario'])) {
			$config = $this->config(base_url().'perfil/muro/'.$id, $this->users_comments_model->filas($id), 5, 3, 4);          
			$comments = $this->users_comments_model->total_paginados($config['per_page'],$this->uri->segment(4),$id); 
			$this->data['comments'] = array();
			foreach($comments as $comment) {
				$emisor = $this->users_model->read($comment->id_emisor);
			 	$comentario = array(
				 	'id_comment' => $comment->id,
				 	'id_emisor' => $emisor->id,
				    'email_emisor' => $emisor->email,
				    'image_emisor' => $emisor->image,
			        'message' => $comment->message,
			        'fecha' => $comment->fecha,
	        	);
				array_push($this->data['comments'], $comentario);
			}

			$this->load->view('perfil.php', $this->data);
		} else {
			show_404();
		}	
	}

	public function send_comment($id_emisor, $id_receptor) 
	{
		if($id_emisor == $this->data['usuario_conectado']['id']) {
			if($this->existe_user($id_receptor)) {
				$comentario = array(
					'id_emisor' => $id_emisor,
					'id_receptor' => $id_receptor, 
					'message' => $this->input->post('inputComentario'),
					'fecha' => date('Y-m-d H-i-s'),
				);
				$this->users_comments_model->insert($comentario);
				redirect('perfil/muro/' . $id_receptor);
			} else {
				redirect('user/force_logout');
			}
		} else {
			redirect('user/force_logout');
		}
	}

	public function delete_comment($id_comment) 
	{
		$comment = $this->users_comments_model->read($id_comment);
		if(!empty($comment) && (($this->data['usuario_conectado']['id'] == $comment->id_emisor) || ($this->data['usuario_conectado']['id'] == $comment->id_receptor))) {
			$this->users_comments_model->delete($id_comment);
			redirect('perfil/muro/' . $comment->id_receptor);
		} else {
			redirect('user/force_logout');
		}
	}

	public function vote_comment($id_comment, $id_user) 
	{
		$comment = $this->users_comments_model->read($id_comment);
		$comentario = array(
			'id' => $comment->id,
			'id_emisor' => $comment->id_emisor,
			'id_receptor' => $comment->id_receptor, 
			'message' => $comment->message,
			'fecha' => $comment->fecha,
		);
		$this->users_comments_model->update($comentario);
		redirect('perfil/muro/' . $comment->id_receptor);
	}

	public function juegos($id) 
	{
		$this->inicializar($id,'juegos');
		if(!empty($this->data['usuario'])) {	
			$this->data['plataformas'] = array();
			$this->data['estados'] = array();        
	        $config = $this->config(base_url().'perfil/juegos/'.$id, $this->games_user_model->filas($id), 8, 3, 4);       
	        $userGames = $this->games_user_model->total_paginados($config['per_page'],$this->uri->segment(4),$id); 
	        $this->data["listadoJuegos"] = $userGames;
			$this->load->view('perfil.php', $this->data);
		} else {
			show_404();
		}
	}

	public function juegos_filtrados($id) 
	{
		$this->inicializar($id,'juegos');
		if(!empty($this->data['usuario'])) {
			
			$this->data['plataformas'] = $plataformas = $this->input->post('plataformas');
	        $this->data['estados'] = $estados = $this->input->post('estados');
	        	if($estados == '') { 
	       		$estados = null;
	       	}
	       	if($plataformas == '') {
	       		$plataformas = null;
	       	}
   	
	        $config = $this->config(base_url().'perfil/juegos_filtrados/'.$id, $this->games_user_model->filas_filtradas($id, $estados, $plataformas), 8, 3, 4);          
	        $userGames = $this->games_user_model->total_paginados_filtrados($config['per_page'],$this->uri->segment(4),$id, $estados, $plataformas); 
	        $this->data["listadoJuegos"] = $userGames;
			$this->load->view('perfil.php', $this->data);
		} else {
			show_404();
		}
	}

	private function config($base_url, $total_rows, $per_page, $num_links, $uri_segment) 
	{
		$this->load->library('pagination'); //Cargamos la librería de paginación
		$config['base_url'] = $base_url; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	    $config['total_rows'] = $total_rows;//calcula el número de filas  
	    $config['per_page'] = $per_page; //Número de registros mostrados por páginas
	    $config['num_links'] = $num_links; //Número de links mostrados en la paginación
	    $config["uri_segment"] = $uri_segment;//el segmento de la paginación
	    $this->pagination->initialize($config); //inicializamos la paginación
	    return $config;
	}

	public function dejar_seguir($id_user, $pagina) 
	{
		$relacion = $this->users_follow_model->esSeguidor($id_user,$this->data['usuario_conectado']['id']);
		if($this->data['usuario_conectado']['id'] != $id_user && $relacion != null && $this->existe_user($id_user)) {
			$this->users_follow_model->delete($relacion->id);
		} else {
			redirect('user/force_logout');
		}
		$this->redirige($id_user, $pagina);
	}

	public function seguir($id_user, $pagina) 
	{
		$relacion = $this->users_follow_model->esSeguidor($id_user,$this->data['usuario_conectado']['id']);
		$nuevaRelacion = array(
			'id_user_seguido' => $id_user,
			'id_user_seguidor' => $this->data['usuario_conectado']['id'], 
		);
		if($this->data['usuario_conectado']['id'] != $id_user && $relacion == null && $this->existe_user($id_user)) {
			if(empty($this->users_block_model->esBloqueador($id,$this->data['usuario_conectado']['id']))) {
				$this->users_follow_model->insert($nuevaRelacion);
			} else {
				redirect('user/force_logout');
			}
		} else {
			redirect('user/force_logout');
		}
		$this->redirige($id_user, $pagina);	
	}

	public function desbloquear($id_user, $pagina) 
	{
		$relacion = $this->users_block_model->estaBloqueado($id_user,$this->data['usuario_conectado']['id']);
		if($this->data['usuario_conectado']['id'] != $id_user && $relacion != null && $this->existe_user($id_user)) {
			$this->users_block_model->delete($relacion->id);
		} else {
			redirect('user/force_logout');
		}
		$this->redirige($id_user, $pagina);
	}

	public function bloquear($id_user, $pagina) 
	{
		$relacion = $this->users_block_model->estaBloqueado($id_user,$this->data['usuario_conectado']['id']);
		$nuevaRelacion = array(
			'id_user_bloqueado' => $id_user,
			'id_user_bloqueador' => $this->data['usuario_conectado']['id'], 
		);
		if($this->data['usuario_conectado']['id'] != $id_user && $relacion == null && $this->existe_user($id_user)) {
			$this->users_block_model->insert($nuevaRelacion);
			$relacion = $this->users_follow_model->esSeguidor($this->data['usuario_conectado']['id'], $id_user);
			if(!empty($relacion)) {
				$this->users_follow_model->delete($relacion->id);
			}
		} else {
			redirect('user/force_logout');
		}
		$this->redirige($id_user, $pagina);	
	}

	private function redirige($id_user, $pagina)
	{
		if($pagina != 'muro' && $pagina != 'social' && $pagina != 'juegos') {
			$this->session->set_flashdata('errorFollowPagina', true);
			redirect('perfil/muro/' . $id_user);	
		} else {
			redirect('perfil/' . $pagina . '/' . $id_user);	
		}
	}

	public function social($id) 
	{
		$this->inicializar($id,'social');
		if(!empty($this->data['usuario'])) {
			$seguidos = $this->users_follow_model->readAllSeguidos($id);
			$seguidores = $this->users_follow_model->readAllSeguidores($id);
			$this->data['listadoSeguidos'] = array();
			$this->data['listadoSeguidores'] = array();
			foreach($seguidos as $seguido) {
				$user = $this->users_model->read($seguido->id_user_seguido);
				$user_seguido = array(
					'id' => $user->id,
					'email' => $user->email,
					'image' => $user->image,
				);
				array_push($this->data['listadoSeguidos'], $user_seguido);
			}
			foreach($seguidores as $seguidor) {
				$user = $this->users_model->read($seguidor->id_user_seguidor);
				$user_seguidor = array(
					'id' => $user->id,
					'email' => $user->email,
					'image' => $user->image,
				);
				array_push($this->data['listadoSeguidores'], $user_seguidor);
			}
			$this->load->view('perfil.php', $this->data);
		} else {
			show_404();
		}
	}

	public function send_message($id_receptor, $pagina) 
	{
		if($this->existe_user($id_receptor)) {
			$message = array(
				'id_user' => $this->data['usuario_conectado']['id'],
				'id_conversacion' => 0, 
				'message' => $this->input->post('inputMessage'),
				'fecha' => date('Y-m-d H-i-s'),
			);
			$existe_conversacion = $this->conversaciones_model->readByUsers($this->data['usuario_conectado']['id'], $id_receptor);
			if(empty($existe_conversacion)) {
				$conversacion = array(
					'user_one' => $this->data['usuario_conectado']['id'],
					'user_two' => $id_receptor, 
				);
				$message['id_conversacion'] = $this->conversaciones_model->insert($conversacion);
			} else {
				$message['id_conversacion'] = $existe_conversacion->id;
			}
			$this->conversacion_messages_model->insert($message);
			$this->redirige($id_receptor, $pagina);
		} else {
			redirect('user/force_logout');
		}
	}

	private function existe_user($id_user) {
		if($this->users_model->read($id_user) != null)
			return true;
		else
			return false;
	}

	public function autocompletar()
	{
		if($this->input->is_ajax_request() && $this->input->post('email'))
        {
			$query = $this->users_model->buscador($this->security->xss_clean($this->input->post('email')));
			if($query != null) {
				?> <div class="list-group"> <?php 
	        	foreach($query as $fila) {
					$user_image = array(
	                  'src' => 'resources/img/users/' . $fila->image,
	                  'height' => '15px',
	                  'width' => '15px',
	                  'alt' => 'userIcon',
	                  );
					?> <a href="<?=site_url('perfil/muro/'.$fila->id)?>" class="list-group-item" name="<?php echo 'usuario_'.$fila->id ?>"><?=img($user_image) ?> <?php echo $fila->email ?></a>	<?php 
				}
				?> </div> <?php
	        }
	        else {
	        	?>
					<a href="" class="list-group-item">
                    <h5 class="list-group-item-heading">No se ha encontrado nada.</h5>
                    </a>
				<?php
	        }
		}
	}
}