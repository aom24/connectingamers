<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() 
	{
		parent::__construct();;
		$this->load->model('users_model', '', TRUE);
		$this->load->helper('email');
		$this->load->helper('security');
	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		redirect('index');
	}

	public function force_logout()
	{
		$this->session->unset_userdata('email');
		$this->session->set_flashdata('errorAccionProhibida', true);
		redirect('index');
	}

	public function log() 
	{
		if($this->users_model->comprueba_login($this->input->post('emailL'), do_hash($this->input->post('passwordL'),'md5'))) {
			$user = $this->users_model->readByEmail($this->input->post('emailL'));
			$sesion = array(
			    'id' => $user->id, 
		        'email' => $this->input->post('emailL'),
		        'password' => $user->password,
		        'name' => $user->name,
			    'surname' => $user->surname,
			    'birthday' => $user->birthday,
		        'signup_date' => $user->signup_date,
		        'role' => $user->role,
		        'image' => $user->image,
	        );
	        $this->session->set_userdata($sesion);
	        $this->session->set_flashdata('successLogin', true);
			redirect('tablon');
		} else {
			$this->session->set_flashdata('errorLogin', true);
			redirect('index');
		}
	}

	public function reg()
	{
		if (valid_email($this->input->post('emailR'))) {
			if($this->users_model->email_disponible($this->input->post('emailR'))) {
				$usuario = array(
				   'email' => $this->input->post('emailR') ,
				   'password' => do_hash($this->input->post('passwordR'), 'md5'),
				   'name' => $this->input->post('nameR') ,
				   'surname' => $this->input->post('surnameR') ,
				   'birthday' => $this->input->post('birthdayR') ,
				   'signup_date' => date('Y-m-d H-i-s') ,
				   'role' => 'user',
				   'image' => "user.jpg"
				);
				if($this->users_model->insert($usuario)) {
					$this->session->set_flashdata('successRegistro', true);
					redirect('index');
				}
			} else {
				$this->session->set_flashdata('errorRegistro', true);
				redirect('index');
			} 
		} else {
		    $this->session->set_flashdata('errorRegistroMailInvalido', true);
			redirect('index');
		}
	}
}