<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administracion extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('users_model', '', TRUE);
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('Grocery_CRUD');
		if ( ! $this->session->userdata('email') != '' || $this->session->userdata('role') != 'admin') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	            redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
	}

	public function news() 
	{
		    $crud = new Grocery_CRUD();
			$crud->set_table('news');
			$crud->set_subject('noticia');
			$crud->change_field_type('description', 'text');
			$crud->display_as('title', 'Titulo');		
			$crud->display_as('description', 'Descripción');
			$crud->display_as('id_user', 'Usuario');
			$crud->display_as('date', 'Fecha');
			$crud->required_fields('title', 'description', 'id_user', 'date');
			$crud->set_relation('id_user','users','email');

			$output = $crud->render();

			$this->data['menu'] = 'news';
			$this->load->vars($this->data);
			$this->load->view('admin', $output);
	}

	public function games() 
	{
		    $crud = new Grocery_CRUD();
			$crud->set_table('games');
			$crud->set_subject('juego');
			$crud->change_field_type('descripcion', 'text');
			$crud->display_as('titulo', 'Titulo');		
			$crud->display_as('descripcion', 'Descripción');
			$crud->display_as('imagen', 'Imagen');
			$crud->display_as('puntuacion', 'Puntuacion');
			$crud->display_as('genero', 'Genero');
			$crud->display_as('plataforma', 'Plataforma');
			$crud->unset_edit_fields('puntuacion');
			$crud->set_field_upload('imagen','resources/img/games');
			$crud->required_fields('titulo', 'descripcion', 'imagen', 'genero', 'plataforma');
			$crud->unset_add_fields('puntuacion');

			$output = $crud->render();
			
			$this->data['menu'] = 'games';
			$this->load->vars($this->data);
			$this->load->view('admin', $output);
	}

	public function users() 
	{
		    $crud = new Grocery_CRUD();
			$crud->set_table('users');
			$crud->set_subject('usuario');
			$crud->display_as('email', 'Email');		
			$crud->display_as('name', 'Nombre');
			$crud->display_as('surname', 'Apellidos');
			$crud->display_as('birthday', 'Cumpleaños');
			$crud->display_as('role', 'Rol');
			$crud->display_as('image', 'Imagen');
			$crud->set_field_upload('image','resources/img/users');
			$crud->unset_columns('password');
			$crud->edit_fields('role');
			$crud->unset_add();

			$output = $crud->render();
			
			$this->data['menu'] = 'users';
			$this->load->vars($this->data);
			$this->load->view('admin', $output);
	}

	public function comments() 
	{
		    $crud = new Grocery_CRUD();
			$crud->set_table('users_comments');
			$crud->set_subject('comentario');
			$crud->change_field_type('message', 'text');
			$crud->display_as('id_emisor', 'Emisor');		
			$crud->display_as('id_receptor', 'Receptor');
			$crud->display_as('message', 'Comentario');
			$crud->display_as('me_gusta', 'Me gusta');
			$crud->display_as('fecha', 'Fecha');
			$crud->set_relation('id_emisor','users','email');
			$crud->set_relation('id_receptor','users','email');
			$crud->unset_add();
			$crud->unset_edit();

			$output = $crud->render();
			
			$this->data['menu'] = 'comentarios';
			$this->load->vars($this->data);
			$this->load->view('admin', $output);
	}

	public function opiniones() 
	{
		    $crud = new Grocery_CRUD();
			$crud->set_table('users_opiniones');
			$crud->set_subject('opinión');
			$crud->change_field_type('message', 'text');
			$crud->display_as('id_emisor', 'Emisor');		
			$crud->display_as('id_noticia', 'Noticia');
			$crud->display_as('message', 'Opinión');
			$crud->display_as('fecha', 'Fecha');
			$crud->set_relation('id_emisor','users','email');
			$crud->set_relation('id_noticia','news','title');
			$crud->unset_add();
			$crud->unset_edit();

			$output = $crud->render();
			
			$this->data['menu'] = 'opiniones';
			$this->load->vars($this->data);
			$this->load->view('admin', $output);
	}

	public function criticas() 
	{
		    $crud = new Grocery_CRUD();
			$crud->set_table('users_critics');
			$crud->set_subject('crítica');
			$crud->change_field_type('message', 'text');
			$crud->display_as('id_emisor', 'Emisor');		
			$crud->display_as('id_juego', 'Juego');
			$crud->display_as('message', 'Crítica');
			$crud->display_as('fecha', 'Fecha');
			$crud->set_relation('id_emisor','users','email');
			$crud->set_relation('id_juego','games','titulo');
			$crud->unset_add();
			$crud->unset_edit();

			$output = $crud->render();
			
			$this->data['menu'] = 'criticas';
			$this->load->vars($this->data);
			$this->load->view('admin', $output);
	}
}	