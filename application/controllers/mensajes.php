<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mensajes extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('users_model', '', TRUE);
		$this->load->model('conversaciones_model', '', TRUE);
		$this->load->model('conversacion_messages_model', '', TRUE);
		if ( ! $this->session->userdata('email') != '') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	            redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
		$this->data['messages'] = array();
		$this->data['conversaciones'] = array();
		$this->data['id_amigo'] = null;
		$this->data['desactivar_boton'] = false;
	}

	public function index() 
	{
		$conversaciones = $this->conversaciones_model->readAllbyUser($this->data['usuario_conectado']['id']);
		foreach($conversaciones as $conversacion) {
			if($conversacion->user_one != $this->data['usuario_conectado']['id']) {
				$amigo = $this->users_model->read($conversacion->user_one);
			} else {
				$amigo = $this->users_model->read($conversacion->user_two);
			}
		 	$linea = array(
		 	'id_conversacion' => $conversacion->id,
		 	'id_amigo' => $amigo->id,
		    'email_amigo' => $amigo->email,
		    'image_amigo' => $amigo->image,
		    'active' => false,
        	);
			array_push($this->data['conversaciones'], $linea);
		}
		$this->data['desactivar_boton'] = true;
		$this->load->view('mensajes.php', $this->data);
	}

	public function conversacion($id_conversacion) 
	{
		$conversacion = $this->conversaciones_model->read($id_conversacion);
		if(!empty($conversacion)) {
			if($conversacion->user_one == $this->data['usuario_conectado']['id'] || $conversacion->user_two == $this->data['usuario_conectado']['id']) {
				$conversaciones = $this->conversaciones_model->readAllbyUser($this->data['usuario_conectado']['id']);
				foreach($conversaciones as $conversacion) {
					if($conversacion->user_one != $this->data['usuario_conectado']['id']) {
						$amigo = $this->users_model->read($conversacion->user_one);
					} else {
						$amigo = $this->users_model->read($conversacion->user_two);
					}
				 	$linea = array(
				 	'id_conversacion' => $conversacion->id,
				 	'id_amigo' => $amigo->id,
				    'email_amigo' => $amigo->email,
				    'image_amigo' => $amigo->image,
				    'active' => false,
		        	);

				 	if($conversacion->id == $id_conversacion) {
				 		$linea['active'] = true;
				 	}

					array_push($this->data['conversaciones'], $linea);
				}

				$messages = $this->conversacion_messages_model->readAllbyConversacion($id_conversacion);

				foreach($messages as $message) {
					$usuario = $this->users_model->read($message->id_user);
				 	$linea = array(
				 	'id_message' => $message->id,
				 	'message' => $message->message,
				 	'id_user' => $usuario->id,
				    'email_user' => $usuario->email,
				    'image_user' => $usuario->image,
				    'fecha_message' => $message->fecha,
		        	);
					array_push($this->data['messages'], $linea);
				}
			} else {
				$this->session->set_flashdata('errorConversacionAjena', true);
			}
		} else {
			show_404();
		}
		$this->load->view('mensajes.php', $this->data);
	}

	public function send_message($id_receptor) 
	{
		$message = array(
			'id_user' => $this->data['usuario_conectado']['id'],
			'id_conversacion' => 0, 
			'message' => $this->input->post('inputMessage'),
			'fecha' => date('Y-m-d H-i-s'),
		);
		$existe_conversacion = $this->conversaciones_model->readByUsers($this->data['usuario_conectado']['id'], $id_receptor);
		if(empty($existe_conversacion)) {
			$conversacion = array(
				'user_one' => $this->data['usuario_conectado']['id'],
				'user_two' => $id_receptor, 
			);
			$message['id_conversacion'] = $this->conversaciones_model->insert($conversacion);
		} else {
			$message['id_conversacion'] = $existe_conversacion->id;
		}
		$this->conversacion_messages_model->insert($message);
		redirect('mensajes/' . $message['id_conversacion']);
	}
}