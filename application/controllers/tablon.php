<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tablon extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('users_model', '', TRUE);
		$this->load->model('games_model', '', TRUE);
		$this->load->model('news_model', '', TRUE);
		$this->load->model('users_critics_model', '', TRUE);
		$this->load->model('users_follow_model', '', TRUE);
		$this->load->model('users_opiniones_model', '', TRUE);
		if ( ! $this->session->userdata('email') != '') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	            redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
	}

	public function index()
	{	
		$criticas = $this->users_critics_model->readLastWithLimit(3);
		$seguidores = $this->users_follow_model->readSeguidoresWithLimit(5,$this->data['usuario_conectado']['id']);
		$noticias = $this->news_model->readAllWithLimit(4);
		$this->data['listadoJuegos'] = $this->games_model->readAllWithLimitOrderByCal(6);
		$this->data['listadoCriticas'] = array();
		$this->data['listadoSeguidores'] = array();
		$this->data['listadoNoticias'] = array();
		foreach($criticas as $critica) {
			$emisor = $this->users_model->read($critica->id_emisor);
			$juego_criticado =$this->games_model->read($critica->id_juego);
		 	$opinion = array(
		 	'id_critica' => $critica->id,
		 	'id_juego' => $critica->id_juego,
		 	'id_emisor' => $emisor->id,
		    'email_emisor' => $emisor->email,
		    'image_emisor' => $emisor->image,
		    'titulo_juego' => $juego_criticado->titulo,
		    'image_juego' => $juego_criticado->imagen,
	        'message' => $critica->message,
	        'fecha' => $critica->fecha,
        	);
			array_push($this->data['listadoCriticas'], $opinion);
		}
		foreach($seguidores as $seguidor) {
			$user = $this->users_model->read($seguidor->id_user_seguidor);
			$total_seguidos = $this->users_follow_model->totalSeguidos($seguidor->id_user_seguidor);
			$total_seguidores = $this->users_follow_model->totalSeguidores($seguidor->id_user_seguidor);
			$user_seguidor = array(
				'id' => $user->id,
				'email' => $user->email,
				'image' => $user->image,
				'total_seguidos' => $total_seguidos,
				'total_seguidores' => $total_seguidores,
			);
			array_push($this->data['listadoSeguidores'], $user_seguidor);
		}
		foreach($noticias as $noticia) {
		 	$new = array(
		 	'id' => $noticia->id,
		 	'title' => $noticia->title,
		 	'description' => $noticia->description,
		    'date' => $noticia->date,
	        'num_comments' => $this->num_comments($noticia->id),
        	);
			array_push($this->data['listadoNoticias'], $new);
		}
		$this->load->view('tablon.php', $this->data);
	}

	private function num_comments($id_new) 
	{
		return $this->users_opiniones_model->opinionesByNoticia($id_new);
	}
}	