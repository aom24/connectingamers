<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticia extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('news_model', '', TRUE);
		$this->load->model('users_model', '', TRUE);
		$this->load->model('users_opiniones_model', '', TRUE);
		if ( ! $this->session->userdata('email') != '') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	            redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
	}

	public function index() 
	{
		$noticias = $this->news_model->readAllbyDate();
		$this->data['news'] = array();
		foreach($noticias as $noticia) {
		 	$new = array(
		 	'id' => $noticia->id,
		 	'title' => $noticia->title,
		 	'description' => $noticia->description,
		    'date' => $noticia->date,
	        'num_comments' => $this->num_comments($noticia->id),
        	);
			array_push($this->data['news'], $new);
		}

		$this->load->view('noticias.php', $this->data);
	}

	private function num_comments($id_new) 
	{
		return $this->users_opiniones_model->opinionesByNoticia($id_new);
	}

	public function info($id_new) 
	{
		$this->data['new'] = $this->news_model->read($id_new);
		if(empty($this->data['new'])) {
			show_404();
		} else {
			$this->data['escritor'] = $this->users_model->read($this->data['new']->id_user);
			$opiniones = $this->users_opiniones_model->readByNoticia($id_new);
			$this->data['opiniones'] = array();
			foreach($opiniones as $opinion) {
				$emisor = $this->users_model->read($opinion->id_emisor);
			 	$opinion = array(
			 	'id_opinion' => $opinion->id,
			 	'id_emisor' => $emisor->id,
			    'email_emisor' => $emisor->email,
			    'image_emisor' => $emisor->image,
		        'message' => $opinion->message,
		        'fecha' => $opinion->fecha,
	        	);
				array_push($this->data['opiniones'], $opinion);
			}
			$this->load->view('noticia.php', $this->data);
		}
	}

	public function delete_opinion($id_noticia, $id_opinion) 
	{
		$opinion = $this->users_opiniones_model->read($id_opinion);
		if(($this->session->userdata('id') == $opinion->id_emisor)) {
			$this->users_opiniones_model->delete($id_opinion);
			redirect('noticia/' . $id_noticia);
		} else {
			$this->session->set_flashdata('errorProhibido', true);
			redirect('index');
		}
	}

	public function send_opinion($id_noticia, $id_emisor) 
	{
		$opinion = array(
			'id_emisor' => $id_emisor,
			'id_notica' => $id_noticia, 
			'message' => $this->input->post('inputOpinion'),
			'fecha' => date('Y-m-d H-i-s'),
		);
		$this->users_opiniones_model->insert($opinion);
		redirect('noticia/' . $id_noticia);
	}
}	