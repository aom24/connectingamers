<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracion extends CI_Controller {

	private $data = array();

	function __construct() 
	{
		parent::__construct();
		$this->load->model('users_model', '', TRUE);
		$this->load->helper('security');
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('Grocery_CRUD');
		if ( ! $this->session->userdata('email') != '') { 
	        $allowed = array(
	           
	         );
	        if ( ! in_array($this->router->fetch_method(), $allowed)) {
	           redirect('user/force_logout');
	        }
	    }
	    $this->data['usuario_conectado'] = $this->session->all_userdata();
	}

	public function preferencias() {
		$this->data['menu'] = 'preferencias';
		$this->load->view('configuracion.php', $this->data);
	}

	public function update() 
	{
		if(empty($this->input->post('passwordNU'))) {
			$pass = do_hash($this->input->post('passwordU'), 'md5');
		} else {
			$pass = do_hash($this->input->post('passwordNU'), 'md5');
		}
		$usuario = array(
		   'id' =>  $this->session->userdata('id'),
		   'email' => $this->session->userdata('email') ,
		   'password' =>  $pass ,
		   'name' => $this->input->post('nameU') ,
		   'surname' => $this->input->post('surnameU') ,
		   'birthday' => $this->input->post('birthdayU') ,
		   'signup_date' => $this->session->userdata('signup_date') ,
		   'role' => $this->session->userdata('role'),
		   'image' => $this->session->userdata('image')
		);
	    if($this->data['usuario_conectado']['password'] == do_hash($this->input->post('passwordU'), 'md5')) {
		    $this->users_model->update($usuario);
            $this->session->set_userdata($usuario);
		    $this->session->set_flashdata('updateOk', 'OK');
		} else {
	    	$this->session->set_flashdata('updateOk', 'ERROR');
		}
		redirect('configuracion/preferencias');
	}

	public function updateImage()
	{
		if ($uploadFile_size > 500000) {
		    $this->session->set_flashdata('updateOk', 'SIZE_ERROR');
			redirect('configuracion/preferencias');
		}
		list($width, $height, $type, $attr) = getimagesize($_FILES["uploadFile"]["tmp_name"]);
		if($width != 120 || $height != 120) {
			$this->session->set_flashdata('updateOk', 'WH_ERROR');
			redirect('configuracion/preferencias');
		}
		$target_dir = "resources/img/users/" . $this->session->userdata('email') . ".jpg";
		if (move_uploaded_file($_FILES["uploadFile"]["tmp_name"], $target_dir)) {
		    $usuario = array(
			   'id' =>  $this->session->userdata('id'),
			   'email' => $this->session->userdata('email') ,
			   'password' => $this->session->userdata('password') ,
			   'name' => $this->session->userdata('name') ,
			   'surname' => $this->session->userdata('surname') ,
			   'birthday' => $this->session->userdata('birthday') ,
			   'signup_date' => $this->session->userdata('signup_date') ,
			   'role' => $this->session->userdata('role'),
			   'image' => $this->session->userdata('email') . ".jpg"
			);

			$this->users_model->update($usuario);
            $this->session->set_userdata($usuario);
            $this->session->set_flashdata('updateOk', 'IMAGE_OK');
		} else {
		    $this->session->set_flashdata('updateOk', 'IMAGE_ERROR');
		}
		redirect('configuracion/preferencias');	
	}

	public function privacidad() {
		$crud = new Grocery_CRUD();
		$crud->where('id_user_bloqueador',$this->data['usuario_conectado']['id']);
		$crud->set_table('users_block');
		$crud->set_subject('bloqueados');
		$crud->display_as('id_user_bloqueado', 'Usuario bloqueado');
		$crud->set_relation('id_user_bloqueado','users','email');
		$crud->columns('id_user_bloqueado');
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_read();
		$crud->unset_print();
		$crud->unset_export();

		$output = $crud->render();

		$this->data['menu'] = 'privacidad';
		$this->load->vars($this->data);
		$this->load->view('configuracion.php', $output);
	}
}	