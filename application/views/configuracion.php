<?php 
$this->load->view("inc/configuracion/cabecera_configuracion"); 
?>
<br>
<div class="container">
  <div class="row">
    <?=$this->load->view("inc/configuracion/menu_configuracion"); ?>
  </div>
  <div class="row">
    <div class="col-md-8">
      <div class="well">
        <?php 
          if($menu == "preferencias") {
            $this->load->view("inc/configuracion/configuracion_preferencias");
          } else if($menu == "privacidad") {
            $this->load->view("inc/configuracion/configuracion_privacidad");
          }
        ?>
      </div>
    </div>
  </div>
</div>
<?php 
$this->load->view("inc/pie_privada"); 
?> 

<script type="text/javascript">

</script>