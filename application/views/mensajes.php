<?php 
   $this->load->view("inc/cabecera_privada"); 
   ?>
<br>
<div class="container">
   <div class="row">
      <div class="col-md-3 col-xs-12">
         <ul class="list-group scroll-area">
            <?php foreach($conversaciones as $conversacion) { $conver = $conversacion['id_conversacion'];?>
            <a href="<?=site_url("mensajes/$conver");?>" class="list-group-item <?php if($conversacion['active']) { echo ' active'; $id_amigo = $conversacion['id_amigo']; } ?>">
               <li class="media">
                  <div class="pull-left">
                     <?php $amigo_image = array(
                        'class'=> 'media-object img-rounded',
                        'src' => site_url('resources/img/users/'.$conversacion['image_amigo']),
                        'width' => '45px',
                        'alt' => 'userIcon',
                        );  ?>
                     <?=img($amigo_image) ?>
                  </div>
                  <div class="media-body">
                     <h5><?=$conversacion['email_amigo'] ?></h5>
                  </div>
               </li>
            </a>
            <?php }?>
         </ul>
      </div>
      <div class="col-md-8">
         <div class="well scroll-area">
            <dl class="dl-horizontal">
               <?php foreach($messages as $message) { ?>
               <p>
               <dt><?=$message['email_user'] ?></dt>
               <dd class="<?php if($usuario_conectado['email'] == $message['email_user']) { echo 'text-primary'; } else { echo ' text-danger'; } ?>"><?=nl2br($message['message']) ?></dd>
               <dd>
                  <h6><small><?=$message['fecha_message'] ?></small></h6>
               </dd>
               </p>
               <?php } ?>
            </dl>
         </div>
         <div class="well">
            <form class="form-horizontal" role="form" id="formMessages" name="formMessages" action="send_message/<?=$id_amigo?>"  method="POST" onsubmit="return revisarMessage()">
               <div class="form-group">
                  <div class="col-md-12">
                     <textarea class="form-control" id="inputMessage" name="inputMessage" rows="4" placeholder="" <?php if($desactivar_boton) { echo 'disabled'; } ?>></textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-offset-10 col-md-1">
                     <button type="submit" class="btn btn-primary" <?php if($desactivar_boton) { echo 'disabled="disabled"'; } ?>>Enviar</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorConversacionAjena">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
   <strong>Error.</strong> Está intentando acceder a una conversación ajena.
</div>
<div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorNoExisteConversacion">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
   <strong>Error.</strong> No existe la conversación que busca.
</div>
<div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-info" id="errorMessage">
   <button type="button" class="close">&times;</button>
   <strong>Mensaje en blanco.</strong> No puede escribir un mensaje sin texto alguno.
</div>
<?php 
   $this->load->view("inc/pie_privada"); 
   ?> 
<script type="text/javascript">
   <?php 
      if($this->session->flashdata('errorNoExisteConversacion') == true) {
        echo '$("#errorNoExisteConversacion").show(400);';
      } else if($this->session->flashdata('errorConversacionAjena') == true) {
        echo '$("#errorConversacionAjena").show(400);';
      }
      ?>
   
   function revisarMessage() {
    if(document.getElementById("inputMessage").value.trim() == '') { $('#errorMessage').show(400); return false; }
    else { return true; }
   }
        
   $('.close').click(function() { $('.alert').hide(400)})
   
</script>