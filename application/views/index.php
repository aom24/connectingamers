<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?='ConnectinGamers'?></title>
      <?=link_tag('resources/css/bootstrap.min.css')?>
      <?=link_tag('resources/css/datepicker.css"')?>
      <?=link_tag('resources/css/bootstrap-social.css')?>
      <?=link_tag('resources/css/font-awesome.min.css')?>
      <?=link_tag('resources/css/signin.css')?>
      <style>
         .datepicker{z-index:1151;}
      </style>
   </head>
   <body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
            </div>
            <!--/.navbar-collapse -->
            <div class="navbar-collapse collapse">
               <form class="navbar-form navbar-right" role="form" action="user/log" method="POST" onsubmit="return revisarLogin()">
                  <div class="form-group">
                     <input type="text" placeholder="Email" id="emailL" name="emailL" class="form-control">
                  </div>
                  <div class="form-group">
                     <input type="password" placeholder="Password" id="passwordL" name="passwordL" class="form-control">
                  </div>
                  <button type="submit" class="btn btn-success">Entrar</button>
                  <a class="btn btn-warning" data-toggle="modal" data-target="#modalRegistro">Registrarse</a>
               </form>
            </div>
            <!--/.navbar-collapse -->
         </div>
      </div>
      <div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorLogin">
         <button type="button" class="close">&times;</button>
         <strong>Datos incorrectos.</strong> Inserte un usuario y contraseña válidos.
      </div>
      <div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-warning" id="errorCamposVaciosLogin">
         <button type="button" class="close">&times;</button>
         <strong>Campos insuficientes.</strong> Debe introducir email y password.
      </div>
      <div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorProhibido">
         <button type="button" class="close">&times;</button>
         <strong>Acceso denegado.</strong> Debe ingresar con un usuario para acceder a la parte privada.
      </div>
      <div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorAccionProhibida">
         <button type="button" class="close">&times;</button>
         <strong>Acción prohibida.</strong> Ha intentado realizar una acción no permitida y se le ha deslogueado por precaución.
      </div>
      <div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-success" id="successRegistro">
         <button type="button" class="close">&times;</button>
         <strong>Registro completo.</strong> Ahora es miembro de ConnectinGamers, bienvenido.
      </div>
      <div class="jumbotron">
         <div class="container">
            <h1><img src="<?php echo $this->config->base_url() . "resources/img/logo_mario.png" ?>" alt="logoCG"> ConnectinGamers</h1>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-md-4">
               <div align="center"><img src="<?=site_url('resources/img/biblioteca_icon.png') ?>" alt="bibliotecaIcon" width="150px" height="150px"></div>
               <h2 align="center">Biblioteca de juegos</h2>
               <p align="justify">Manten tu biblioteca de juegos actualizada y compartela con tus amigos para que sepan que juegos has jugado, cuales estas jugando y cuales te gustaría poder jugar.</p>
            </div>
            <div class="col-md-4">
               <div align="center"><img src="<?=site_url('resources/img/noticias_icon.png') ?>" alt="bibliotecaIcon" width="150px" height="150px"></div>
               <h2 align="center">Noticias de actualidad</h2>
               <p align="justify">Estate al día de las últimas noticias sobre tus juegos favoritos, no dejes que se te escape ninguna novedad que ocurra en el mundillo.</p>
            </div>
            <div class="col-md-4">
               <div align="center"><img src="<?=site_url('resources/img/amigos_icon.png') ?>" alt="bibliotecaIcon" width="150px" height="150px"></div>
               <h2 align="center">Nuevas amistades</h2>
               <p align="justify">Encuentra gente con las mismas aficiones y gustos que tu, comenta tus experiencias, y hazte participe del buen rollo que se respira aquí.</p>
            </div>
         </div>
         <hr>
         <footer>
            <p>ConnectinGamers © Álvaro Outeiro 2014/2015</p>
         </footer>
      </div>

      <script src="<?=site_url('resources/js/jquery-2.1.1.min.js')?>"></script>
      <script src="<?=site_url('resources/js/bootstrap-datepicker.js')?>"></script>
      <script src="<?=site_url('resources/js/bootstrap.min.js')?>"></script>
      
      <div class="modal fade" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Registro <span class="label label-primary">Campos obligatorios</span></h4>
               </div>
               <form id="formularioRegistro" name="formularioRegistro" action="user/reg" method="POST" onsubmit="return revisarRegistro()">
                  <div class="modal-body">
                     <div class="alert alert-info" id="errorCamposVaciosRegistro" style="display: none">
                        <button type="button" class="close">&times;</button>
                        <strong>Campos insuficientes.</strong> Debe completar todos los campos obligatorios.
                     </div>
                     <div class="alert alert-danger" id="errorPassword" style="display: none">
                        <button type="button" class="close">&times;</button>
                        <strong>Contraseñas diferentes.</strong> Las contraseñas deben coincidir.
                     </div>
                     <div class="alert alert-danger" id="errorRegistro" style="display: none">
                        <button type="button" class="close">&times;</button>
                        <strong>Email ya registrado.</strong> Ya existe un usuario con ese email, por favor introduzca otro.
                     </div>
                     <div class="alert alert-danger" id="errorRegistroMailInvalido" style="display: none">
                        <button type="button" class="close">&times;</button>
                        <strong>Email inválido.</strong> Debe introducir un email váido.
                     </div>
                     <div class="form-group">
                        <h4><span class="label label-primary">Email</span></h4>
                        <input type="email" class="form-control" id="emailR" name="emailR" placeholder="Introduzca email">
                     </div>
                     <div class="form-group">
                        <h4><span class="label label-primary">Password</span></h4>
                        <input type="password" class="form-control" id="passwordR" name="passwordR" placeholder="Introduzca password">
                     </div>
                     <div class="form-group">
                        <h4><span class="label label-primary">Repita Password</span></h4>
                        <input type="password" class="form-control" id="repitaPasswordR" name="repitaPasswordR" placeholder="Repita password">
                     </div>
                     <div class="form-group">
                        <h4><span class="label label-default">Nombre</span></h4>
                        <input type="text" class="form-control" id="nameR" name="nameR" placeholder="Introduzca nombre">
                     </div>
                     <div class="form-group">
                        <h4><span class="label label-default">Apellidos</span></h4>
                        <input type="text" class="form-control" id="surnameR" name="surnameR" placeholder="Introduzca apellidos">
                     </div>
                     <div class="form-group">
                        <h4><span class="label label-default">Fecha de nacimiento</span></h4>
                        <input type="text" class="form-control" id="birthdayR" name="birthdayR" placeholder="Introduzca fecha">
                     </div>
                  </div>
                  <div class="modal-footer">
                     <input type="submit" class="btn btn-primary" value="Registrar"/>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <script type="text/javascript" language="javascript">
         $(document).ready(function() {
                 
         })
         
         $(function() {
           $('#birthdayR').datepicker({
             format: 'yyyy-mm-dd'})
         });
         
         function revisarRegistro() {
           if(document.getElementById("passwordR").value != document.getElementById("repitaPasswordR").value) { $('#errorPassword').show(400); return false; }
           else if(document.getElementById("emailR").value == "" || document.getElementById("passwordR").value == "" || document.getElementById("repitaPasswordR").value == "") { $('#errorCamposVaciosRegistro').show(400); return false;}
           else { return true; }
         }
         
         function revisarLogin() {
           if(document.getElementById("emailL").value == "" || document.getElementById("passwordL").value == "") { $('#errorCamposVaciosLogin').show(400); return false; }
           else { return true; }
         }
         
         <?php 
            if($this->session->flashdata('errorLogin') == true) {
              echo '$("#errorLogin").show(400);';
            }
            else if($this->session->flashdata('errorRegistro') == true) {
              echo '$("#modalRegistro").modal("show");';
              echo '$("#errorRegistro").show(400);';
            }
            else if($this->session->flashdata('errorRegistroMailInvalido') == true) {
              echo '$("#modalRegistro").modal("show");';
              echo '$("#errorRegistroMailInvalido").show(400);';
            }
            else if($this->session->flashdata('errorProhibido') == true) {
              echo '$("#errorProhibido").show(400);';
            }
            else if($this->session->flashdata('successRegistro') == true) {
              echo '$("#successRegistro").show(400);';
            }
            else if($this->session->flashdata('errorAccionProhibida') == true) {
              echo '$("#errorAccionProhibida").show(400);';
            }
            ?>
         
         $('.close').click(function() { $('.alert').hide(400)})
         
      </script>
   </body>
</html>