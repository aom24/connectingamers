<?php $this->load->view("inc/cabecera_privada"); ?>
<br><br>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <div class="col-md-3">
            <?php $escritor_imagen = array(
               'class'=> 'img-thumbnail',
               'src' => site_url('resources/img/users/'.$escritor->image),
               'data-toggle' => 'tooltip',
               'data-placement' => 'bottom',
               'title' => $escritor->email,
               'alt' => 'userIcon',
               );  ?>
            <?=img($escritor_imagen) ?>
         </div>
         <div class="col-md-9">
            <h1> <?=$new->title ?></h1>
            <small> <?=$new->date ?></small>
         </div>
         <?=$new->description ?>
      </div>
      <div class="col-md-6">
         <h2>Comentarios</h2>
         <div class="alert alert-info" id="errorOpinion" style="display: none">
            <button type="button" class="close">&times;</button>
            <strong>Comentario en blanco.</strong> No se pueden enviar comentarios sin texto alguno.
         </div>
         <div class="well scroll-area">
            <ul class="media-list">
               <?php foreach($opiniones as $opinion) { ?>
               <li class="media ">
                  <?php $user_comenta_image = array(
                     'class'=> 'media-object img-circle',
                     'src' => site_url('resources/img/users/'.$opinion['image_emisor']),
                     'height' => '40px',
                     'width' => '40px',
                     'alt' => 'userIcon',
                     );  ?>
                  <?=anchor('perfil/muro/'.$opinion['id_emisor'],img($user_comenta_image),'class="pull-left"') ?>
                  <?php if($usuario_conectado['id'] == $opinion['id_emisor']) {
                     echo anchor('noticia/delete_opinion/' . $new->id . '/' . $opinion['id_opinion'],'<span class="glyphicon glyphicon-remove"></span>','class="pull-right"');    
                     }?>
                  <div class="media-body">
                     <h5 class="media-heading"><?=anchor('perfil/muro/'.$opinion['id_emisor'],$opinion['email_emisor']) ?></h5>
                     <p><?=nl2br($opinion['message']) ?></p>
                     <h6><small><?=$opinion['fecha'] ?></small></h6>
                  </div>
               </li>
               <?php } ?>
            </ul>
         </div>
         <div class="well">
            <form class="form-horizontal" role="form" id="formMuro" name="formMuro" action="<?='send_opinion/' . $new->id . '/' . $usuario_conectado['id'] ?>"  method="POST" onsubmit="return revisarOpinion()">
               <div class="form-group">
                  <div class="col-md-12">
                     <textarea class="form-control" id="inputOpinion" name="inputOpinion" rows="4" placeholder="Comenta la noticia..."></textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-offset-10 col-md-1">
                     <button type="submit" class="btn btn-primary">Enviar</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<br><br>
<?php $this->load->view("inc/pie_privada"); ?>
<script type="text/javascript" language="javascript">
   $(document).ready(function() {
           
   })
   
   $(function () {
     $('[data-toggle="tooltip"]').tooltip()
   })
   
   function revisarOpinion() {
      if(document.getElementById("inputOpinion").value.trim() == '') { $('#errorOpinion').show(400); return false; }
      else { return true; }
    }
   
    $('.close').click(function() { $('.alert').hide(400)})
</script>