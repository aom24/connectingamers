<?php 
$this->load->view("inc/administracion/cabecera_administracion"); 
?>
<br>
<div class="container">
  <div class="row"> 
    <div class="col-md-12">
      <ul class="nav nav-pills">
        <li role="presentation" id="news"><?=anchor(site_url('administracion/news'),'Noticias') ?></li>
        <li role="presentation" id="games"><?=anchor(site_url('administracion/games'),'Juegos') ?></li>
        <li role="presentation" id="users"><?=anchor(site_url('administracion/users'),'Usuarios') ?></li>
        <li role="presentation" id="comentarios"><?=anchor(site_url('administracion/comments'),'Comentarios') ?></li>
        <li role="presentation" id="criticas"><?=anchor(site_url('administracion/criticas'),'Criticas') ?></li>
        <li role="presentation" id="opiniones"><?=anchor(site_url('administracion/opiniones'),'Opiniones') ?></li>
        </ul>
      <br>
    </div>
    <?=$output ?>
  </div>
</div>

<script type="text/javascript">
  <?php 
  if($menu == 'news') {
    echo '$("#news").attr("class", "active");';
  } else if($menu == 'games') {
    echo '$("#games").attr("class", "active");';
  } else if($menu == 'users') {
    echo '$("#users").attr("class", "active");';
  } else if($menu == 'comentarios') {
    echo '$("#comentarios").attr("class", "active");';
  } else if($menu == 'criticas') {
    echo '$("#criticas").attr("class", "active");';
  } else if($menu == 'opiniones') {
    echo '$("#opiniones").attr("class", "active");';
  }
  ?>
</script>
<?php 
$this->load->view("inc/administracion/pie_administracion"); 
?>