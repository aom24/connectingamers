</div>

<script type="text/javascript">

</script>

<div id="footer">
   <div class="container">
      <p>ConnectinGamers © Álvaro Outeiro 2014/2015 <a style="text-align: right" href="mailto:aom24@alu.ua.es?subject=Bugs en ConnectinGamers">Avisa de bugs</a></p>
   </div>
</div>
<!-- MODAL ENVIAR MENSAJE PRIVADO -->
<div class="modal fade" id="modalPrivado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Mensaje para <b><?=$usuario->email?></b></h4>
         </div>
         <div style="display: none" class="alert alert-info" id="errorMessagePerfil">
            <button type="button" class="close">&times;</button>
            <strong>Mensaje en blanco.</strong> No puede escribir un mensaje sin texto alguno.
         </div>
         <div class="modal-body">
            <form class="form-horizontal" role="form" id="formMuro" name="formMuro" action="<?php echo $this->config->base_url() . 'perfil/send_message/' . $usuario->id . '/' . $menu ?>"  method="POST" onsubmit="return revisarMessage()">
               <div class="form-group">
                  <div class="col-md-12">
                     <textarea class="form-control" id="inputMessage" name="inputMessage" rows="4" placeholder="Escribe tu mensaje."></textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-offset-10 col-md-1">
                     <button type="submit" class="btn btn-primary">Enviar</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
</body>
</html>

