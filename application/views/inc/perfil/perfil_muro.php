<div class="alert alert-info" id="errorComentario" style="display: none">
   <button type="button" class="close">&times;</button>
   <strong>Comentario en blanco.</strong> No se pueden enviar comentarios sin texto alguno.
</div>
<form class="form-horizontal" role="form" id="formMuro" name="formMuro" action="<?=site_url('/perfil/send_comment/' . $usuario_conectado['id'] . '/' . $usuario->id) ?>"  method="POST" onsubmit="return revisarComentario()">
   <div class="form-group">
      <div class="col-md-12">
         <textarea class="form-control" id="inputComentario" name="inputComentario" rows="4" placeholder="Escribe algo a <?=$usuario->email ?>..."></textarea>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-offset-10 col-md-1">
         <button type="submit" class="btn btn-primary">Enviar</button>
      </div>
   </div>
</form>
<ul class="media-list">
   <?php foreach ($comments as $comment) {?>
   <li class="media well well-sm">
      <?php $image_emisor = array(
         'class'=> 'media-object img-circle',
         'src' => site_url('resources/img/users/'.$comment['image_emisor']),
         'height' => '40',
         'width' => '40',
         'alt' => 'userIcon',
         );  ?>
      <?=anchor('perfil/muro/'.$comment['id_emisor'],img($image_emisor),'class="pull-left"')?>
      <?php if (($usuario_conectado['email'] == $usuario->email) || ($usuario_conectado['email'] == $comment['email_emisor'])) {
         echo anchor('perfil/delete_comment/' . $comment['id_comment'],'<span class="glyphicon glyphicon-remove"></span>','class="pull-right"');      
         }?>
      <div class="media-body">
         <h5 class="media-heading">
            <?=anchor('perfil/muro/'.$comment['id_emisor'],$comment['email_emisor'])?>
         </h5>
         <p><?=nl2br($comment['message']) ?></p>
         <h6><small><?=$comment['fecha'] ?></small></h6>
      </div>
   </li>
   <?php } ?>
</ul>
<div class="row">
   <div class="col-md-12">
      <nav>
         <center><?=$this->pagination->create_links()?></center>
      </nav>
   </div>
</div>
<script type="text/javascript" language="javascript">
   $(document).ready(function() {
           
   })
   
   function revisarComentario() {
      if(document.getElementById("inputComentario").value.trim() == '') { $('#errorComentario').show(400); return false; }
      else { return true; }
    }
   
    $('.close').click(function() { $('.alert').hide(400)})
   
</script>