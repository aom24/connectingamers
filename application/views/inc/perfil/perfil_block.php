<?php 
if (!$esBloqueador) {
	echo '<h3><i class="fa fa-lock"></i> Perfil privado. Necesita seguir al usuario para poder ver todo el perfil.</h3>';
} else {
	echo '<h3><i class="fa fa-lock"></i> Perfil bloqueado. El usuario te ha bloqueado.</h3>';
}