<div class="row">
   <div class="col-md-6">
      <div role="tabpanel">
         <!-- Nav tabs -->
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#siguiendo" aria-controls="siguiendo" role="tab" data-toggle="tab">Siguiendo</a></li>
            <li role="presentation"><a href="#seguidores" aria-controls="seguidores" role="tab" data-toggle="tab">Seguidores</a></li>
         </ul>
         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="siguiendo">
               <table class="table table-striped">
                  <tbody>
                     <?php foreach($listadoSeguidos as $seguido) {?>
                     <?php $seguidor_image = array(
                        'class'=> 'media-object img-rounded',
                        'src' => site_url('resources/img/users/'.$seguido['image']),
                        'height' => '55px',
                        'width' => '55px',
                        'alt' => 'userIcon',
                        );  ?>
                     <tr>
                        <td>
                           <div class="media">
                              <div class="media-left">
                                 <?=img($seguidor_image) ?> 
                              </div>
                              <div class="media-body">
                                 <h4 class="media-heading"><?=$seguido['email'] ?></h4>
                                 <?php echo anchor('perfil/muro/'.$seguido['id'],'<span class="glyphicon glyphicon glyphicon-user" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Ver perfil"></span>') ?>
                                 <?php if($usuario_conectado['id'] == $usuario->id) {
                                    echo anchor('perfil/dejar_seguir/'.$seguido['id'].'/social','<span class="glyphicon glyphicon-eye-close" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="No seguir"></span>');
                                    }
                                    ?> 
                              </div>
                           </div>
                        </td>
                     </tr>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="seguidores">
               <table class="table table-striped">
                  <tbody>
                     <?php foreach($listadoSeguidores as $seguidor) {?>
                     <?php $seguidor_image = array(
                        'class'=> 'media-object img-rounded',
                        'src' => site_url('resources/img/users/'.$seguidor['image']),
                        'height' => '55px',
                        'width' => '55px',
                        'alt' => 'userIcon',
                        );  ?>
                     <tr>
                        <td>
                           <div class="media">
                              <div class="media-left">
                                 <?=img($seguidor_image) ?> 
                              </div>
                              <div class="media-body">
                                 <h4 class="media-heading"><?=$seguidor['email'] ?></h4>
                                 <?=anchor('perfil/muro/'.$seguidor['id'],'<span class="glyphicon glyphicon glyphicon-user" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Ver perfil"></span>') ?>
                                 <?php if($usuario_conectado['id'] == $usuario->id) {
                                    echo anchor('perfil/bloquear/'.$seguidor['id'].'/social','<span class="glyphicon glyphicon-ban-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Bloquear"></span>');
                                    }
                                    ?> 
                              </div>
                           </div>
                        </td>
                     </tr>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" language="javascript">
   $(document).ready(function() {
           
   })
   
   $(function () {
     $('[data-toggle="tooltip"]').tooltip()
   })
   
</script>