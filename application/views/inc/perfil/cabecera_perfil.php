<div class="col-md-10">
   <div class="well">
      <div class="media">
         <div class="pull-left">
            <?php $user_image_perfil = array(
               'class'=> 'media-object img-circle',
               'src' => site_url('resources/img/users/'.$usuario->image),
               'alt' => 'userIcon',
               );  ?>
            <?=img($user_image_perfil) ?> 
         </div>
         <div Class="pull-right">
            <?php if ($usuario_conectado['email'] != $usuario->email) {
                    if(!$esBloqueador) {
                       if (!$sigue) {
                          echo anchor(site_url('perfil/seguir/' . $usuario->id . '/' . $menu),'<span class="glyphicon glyphicon-eye-close"></span> Seguir','type="button" class="btn btn-default"');
                         } else {
                          echo anchor(site_url('perfil/dejar_seguir/' . $usuario->id . '/' . $menu),'<span class="glyphicon glyphicon-eye-open"></span> Siguiendo','type="button" class="btn btn-success"');
                         }
                       }
                       echo nbs(1);
                    if(!$estaBloqueado) {
                      echo anchor(site_url('perfil/bloquear/' . $usuario->id . '/' . $menu),'<i class="fa fa-lock"></i> Bloquear','type="button" class="btn btn-default"');
                    } else {
                      echo anchor(site_url('perfil/desbloquear/' . $usuario->id . '/' . $menu),'<i class="fa fa-unlock"></i> Desbloquear','type="button" class="btn btn-danger"');
                    }
                  } 
              ?>
            <img src="<?=site_url("resources/img/logo_sackboy.png") ?>" alt="logoSackBoy">
         </div>
         <div class="media-body">
            <h2 class="media-heading"><?=$usuario->email; ?><small> <?=$usuario->name . ' ' . $usuario->surname ?></small> </h2>
            <?php if ($usuario_conectado['email'] != $usuario->email) {  
               if ($esSeguidor) {
                 echo '<small><span class="label label-default">Te sigue</span></small> ';
                } 
                 echo "<a type='button' class='btn btn-default btn-xs' data-toggle='modal' data-target='#modalPrivado'><span class='glyphicon glyphicon-send'></span> Mensaje privado</a>";
                 
               } ?>
            <p>Miembro de ConnectinGamers desde el <?=substr($usuario->signup_date, 0, 10) ?></p>
            <ul class="list-unstyled pull-left">
               <li><span class="label label-primary"><b><?=$total_seguidores ?></b> SEGUIDORES</span></li>
               <li><span class="label label-primary"><b><?=$total_seguidos ?></b> SIGUIENDO</span> </li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-warning" id="errorFollowPagina">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
   <strong>Aviso.</strong> Ha intentado redirigirse a una página que no existe y se le ha redirigido automáticamente.
</div>