<div id="paginados">
   <?php
      if($listadoJuegos)
      {
          foreach ($listadoJuegos as $juego) 
          {
          $game_image = array(
            'src' => site_url('resources/img/games/'.$juego->imagen),
            'alt' => 'userIcon',
            'class' => 'media-object img-thumbnail',
          );
      ?>
   <div class="col-md-3">
      <?=anchor('juego/'.$juego->id,img($game_image)) ?>
      <center><span class="<?php echo'label '; if ($juego->plataforma == 'PS4') { echo 'label-primary'; } else if ($juego->plataforma == 'XONE') { echo 'label-success'; } else if ($juego->plataforma == 'PC') { echo 'label-default'; } else { echo 'label-info'; }?>">
         <?=$juego->plataforma ?></span> 
         <span class="label <?php echo'label '; if ($juego->estado == 'Jugando') { echo 'label-danger'; } else if ($juego->estado == 'Jugado') { echo 'label-success'; } else  { echo 'label-info'; } ?>">
         <?=$juego->estado ?>
         <span>
      </center>
      <hr>
   </div>
   <?php
      } 
      ?>
</div>
<div class="row">
   <div class="col-md-12">
      <nav>
         <center><?=$this->pagination->create_links()?></center>
      </nav>
   </div>
</div>
<?php
   } else { echo '<h4>El usuario no ha añadido ningún juego a su biblioteca.</h4>'; }
   ?>