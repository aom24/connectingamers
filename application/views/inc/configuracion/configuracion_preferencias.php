<div class="row">
   <div class="col-md-6">
      <form class="form-horizontal" id="formUpdate" name="formUpdate" action="<?='update'?>" method="POST" onsubmit="return revisarUpdate()">
         <div class="form-group">
            <div class="alert alert-info" id="errorCamposVaciosUpdate" style="display: none">
               <button type="button" class="close">&times;</button>
               <strong>Campos insuficientes.</strong> Debe completar todos los campos obligatorios.
            </div>
            <div class="alert alert-danger" id="errorPassword" style="display: none">
               <button type="button" class="close">&times;</button>
               <strong>Contraseñas diferentes.</strong> Las contraseñas deben coincidir.
            </div>
            <div class="alert alert-success" id="updateCorrecto" style="display: none">
               <button type="button" class="close">&times;</button>
               <strong>Usuario actualizado.</strong> Sus datos han sido cambiados satisfactoriamente.
            </div>
            <div class="alert alert-danger" id="updateIncorrecto" style="display: none">
               <button type="button" class="close">&times;</button>
               <strong>Contraseña incorrecta.</strong> Debe introducir su contraseña actual para poder realizar la actualización.
            </div>
         </div>
         <div class="form-group">
            <div class="col-md-10">
               <h4><span class="label label-primary">Password Actual</span></h4>
               <input type="password" class="form-control" id="passwordU" name="passwordU" placeholder="Password actual">
            </div>
         </div>
         <div class="form-group">
            <div class="col-md-10">
               <h4><span class="label label-primary">Password Nueva</span></h4>
               <input type="password" class="form-control" id="passwordNU" name="passwordNU" value="">
            </div>
         </div>
         <div class="form-group">
            <div class="col-md-10">
               <h4><span class="label label-primary">Repita Password Nueva</span></h4>
               <input type="password" class="form-control" id="repitaPasswordNU" name="repitaPasswordNU" value="">
            </div>
         </div>
         <div class="form-group">
            <div class="col-md-10">
               <h4><span class="label label-default">Nombre</span></h4>
               <input type="text" class="form-control" id="nameU" name="nameU" value="<?=$usuario_conectado['name'] ?>">
            </div>
         </div>
         <div class="form-group">
            <div class="col-md-10">
               <h4><span class="label label-default">Apellidos</span></h4>
               <input type="text" class="form-control" id="surnameU" name="surnameU" value="<?=$usuario_conectado['surname'] ?>">
            </div>
         </div>
         <div class="form-group">
            <div class="col-md-10">
               <h4><span class="label label-primary">Fecha de nacimiento</span></h4>
               <input type="text" class="form-control" id="birthdayU" name="birthdayU" value="<?=substr($usuario_conectado['birthday'],0,10) ?>">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-offset-6 col-md-1">
               <input type="submit" class="btn btn-primary" value="Guardar"/>
            </div>
         </div>
      </form>
   </div>
   <div class="col-sm-offset-1 col-md-5">
      <form class="form-horizontal" action="<?='updateImage' ?>" method="POST" enctype="multipart/form-data">
         <div class="form-group">
            <?php $user_image_configuracion = array(
               'class'=> 'media-object img-rounded',
               'src' => site_url('resources/img/users/'.$usuario_conectado['image']),
               'alt' => 'userIcon',
               );  ?>
            <?=img($user_image_configuracion) ?>
         </div>
         <div class="form-group">
            <input type="file" name="uploadFile">
         </div>
         <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Cambiar">
         </div>
         <div class="alert alert-danger" id="errorSize" style="display: none">
            <button type="button" class="close">&times;</button>
            <strong>Tamaño no válido.</strong> La imagen no puede pesar mas de 500kb.
         </div>
         <div class="alert alert-danger" id="errorWH" style="display: none">
            <button type="button" class="close">&times;</button>
            <strong>Dimensiones no válidas.</strong> La imagen debe de ser de 120x120.
         </div>
         <div class="alert alert-danger" id="errorImageUpload" style="display: none">
            <button type="button" class="close">&times;</button>
            <strong>Error.</strong> Se produjo un error intentando almacenar la imagen.
         </div>
         <div class="alert alert-success" id="imageUpload" style="display: none">
            <button type="button" class="close">&times;</button>
            <strong>Imagen subida.</strong> La actualización de su avatar se ha realizado correctamente.
         </div>
      </form>
   </div>
</div>
<script type="text/javascript" language="javascript">
   $(document).ready(function() {
           
   })
   
   $(function() {
     $('#birthdayU').datepicker({
       format: 'yyyy-mm-dd'})
   });
   
   function revisarUpdate() {
     if(document.getElementById("passwordU").value == "" || document.getElementById("birthdayU").value == "") { $('#errorCamposVaciosUpdate').show(400); return false;}
     else if((document.getElementById("passwordNU").value != "" && document.getElementById("repitaPasswordNU").value == "") || (document.getElementById("passwordNU").value == "" && document.getElementById("repitaPasswordNU").value != "")) { $('#errorPassword').show(400); return false; }
     else if(document.getElementById("passwordNU").value != document.getElementById("repitaPasswordNU").value) { $('#errorPassword').show(400); return false; }
     else { return true; 
     }
   }
   
   <?php 
      if($this->session->flashdata('updateOk') == "OK") {
        echo '$("#updateCorrecto").show(400);';
      }
      else if($this->session->flashdata('updateOk') == "ERROR") {
        echo '$("#updateIncorrecto").show(400);';
      }
      else if($this->session->flashdata('updateOk') == "SIZE_ERROR") {
        echo '$("#errorSize").show(400);';
      }
      else if($this->session->flashdata('updateOk') == "WH_ERROR") {
        echo '$("#errorWH").show(400);';
      }
      else if($this->session->flashdata('updateOk') == "IMAGE_OK") {
        echo '$("#imageUpload").show(400);';
      }
      else if($this->session->flashdata('updateOk') == "IMAGE_ERROR") {
        echo '$("#errorImageUpload").show(400);';
      }
      ?>
   
   $('.close').click(function() { $('.alert').hide(400)})
   
</script>