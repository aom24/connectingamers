<script type="text/javascript" language="javascript">
   $(document).ready(function() {
   	<?php 
      if($juego->plataforma == 'PS4') {
      echo '$("#plataforma").attr("class", "label label-primary");';
      }
      else if($juego->plataforma == 'XONE') {
      echo '$("#plataforma").attr("class", "label label-success");';
      }
      else if($juego->plataforma == 'PC') {
      echo '$("#plataforma").attr("class", "label label-default");';
      }
      else {
      	echo '$("#plataforma").attr("class", "label label-info");';
      }
      
      if($usuario_juego['estado'] == 'Pendiente') {
      	echo '$("#userState").attr("class", "btn btn-info dropdown-toggle");';
      }
      else if($usuario_juego['estado'] == 'Jugando') {
      	echo '$("#userState").attr("class", "btn btn-danger dropdown-toggle");';
      }
      else if($usuario_juego['estado'] == 'Jugado') {
      	echo '$("#userState").attr("class", "btn btn-success dropdown-toggle");';
      }
      else {
      	echo '$("#userState").attr("class", "btn btn-default dropdown-toggle");';
      }
      
      if($usuario_juego['puntuacion'] > 1 and $usuario_juego['puntuacion'] <= 2) {
      	echo '$("#e1").css("color", "#E8E162");';
      }
      else if($usuario_juego['puntuacion'] > 2 and $usuario_juego['puntuacion'] <= 4) {
      	echo '$("#e1").css("color", "#E8E162");';
      	echo '$("#e2").css("color", "#E8E162");';
      }
      if($usuario_juego['puntuacion'] > 4 and $usuario_juego['puntuacion'] <= 6) {
      	echo '$("#e1").css("color", "#E8E162");';
      	echo '$("#e2").css("color", "#E8E162");';
      	echo '$("#e3").css("color", "#E8E162");';
      }
      if($usuario_juego['puntuacion'] > 6 and $usuario_juego['puntuacion'] <= 8) {
      	echo '$("#e1").css("color", "#E8E162");';
      	echo '$("#e2").css("color", "#E8E162");';
      	echo '$("#e3").css("color", "#E8E162");';
      	echo '$("#e4").css("color", "#E8E162");';
      }
      if($usuario_juego['puntuacion'] > 8 and $usuario_juego['puntuacion'] <= 10) {
      	echo '$("#e1").css("color", "#E8E162");';
      	echo '$("#e2").css("color", "#E8E162");';
      	echo '$("#e3").css("color", "#E8E162");';
      	echo '$("#e4").css("color", "#E8E162");';
      	echo '$("#e5").css("color", "#E8E162");';
      }
      ?>
   
   })
</script>