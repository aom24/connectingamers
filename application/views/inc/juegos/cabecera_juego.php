<div class="col-xs-4 col-md-2 col-md-offset-1">
   <div class="thumbnail col-xs-12">
      <?=img(site_url('resources/img/games/'.($juego->imagen)))?>
   </div>
   <center>
      <div class="btn-group btn-large">
         <button type="button" id='userState' class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <?=$usuario_juego['estado'] ?> <span class="caret"></span>
         </button>
         <ul class="dropdown-menu" role="menu">
            <li><?=anchor('juego/cambia_estado/'.$juego->id.'/Jugando','Jugando')?></li>
            <li><?=anchor('juego/cambia_estado/'.$juego->id.'/Pendiente','Pendiente')?></li>
            <li><?=anchor('juego/cambia_estado/'.$juego->id.'/Jugado','Jugado')?></li>
            <li><?=anchor('juego/cambia_estado/'.$juego->id.'/Nointeresa','No interesa')?></li>
         </ul>
      </div>
   </center>
   <br>
</div>
<div class="col-md-7">
   <h2>
      <?=$juego->titulo ?> <span class="label label-warning"> <?=$juego->puntuacion; ?></span>&nbsp;
      <div class="ec-stars-wrapper">
         <?=anchor('juego/cambia_puntuacion/'.$juego->id.'/2','&#9733','id = "e1"')?>
         <?=anchor('juego/cambia_puntuacion/'.$juego->id.'/4','&#9733','id = "e2"')?>
         <?=anchor('juego/cambia_puntuacion/'.$juego->id.'/6','&#9733','id = "e3"')?>
         <?=anchor('juego/cambia_puntuacion/'.$juego->id.'/8','&#9733','id = "e4"')?>
         <?=anchor('juego/cambia_puntuacion/'.$juego->id.'/10','&#9733','id = "e5"')?>
      </div>
   </h2>
   </br>
</div>
<div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorEstado">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
   <strong>Error votando.</strong> No puede votar un juego que no este jugando o haya jugado.
</div>
<div style="position: absolute; top:60px; right:150px; display: none" class="alert alert-danger" id="errorPuntuacion">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
   <strong>Error votando.</strong> Ha intentado votar con una cifra no permitida.
</div>