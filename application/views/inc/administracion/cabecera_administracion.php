<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?='ConnectinGamers'?></title>
      <?=link_tag('resources/css/bootstrap.min.css')?>
      <?=link_tag('resources/css/datepicker.css"')?>
      <?=link_tag('resources/css/bootstrap-social.css')?>
      <?=link_tag('resources/css/font-awesome.min.css')?>
      <?=link_tag('resources/css/signin.css')?>
      <?=link_tag('resources/css/estilos.css')?>
      <?php foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?=$file; ?>" />
      <?php endforeach; ?>
      <?php foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
      <?php endforeach; ?>
      <style>
         .datepicker { z-index:1151; }
         .scroll-area {
         height: 65%;
         position: relative;
         overflow: auto;
         }
      </style>
      <script src="<?=site_url('resources/js/bootstrap.min.js')?>"></script>
   </head>
   <body>
      <div id="wrap">
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="<?php echo site_url("tablon");?>" style="color: white">ConnectinGamers</a>
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <?php $user_image = array(
                  'src' => 'resources/img/logo_mario.png',
                  'height' => '25px',
                  'width' => '25px',
                  'alt' => 'userIcon',
                  );  ?>
               <?=img($user_image) ?>
               </button>
            </div>
            <div class="navbar-collapse collapse">
               <ul class="nav navbar-nav navbar-right">
                  <li><a href="<?=site_url('juego')?>" style="color: white">Juegos</a></li>
                  <li><a href="<?=site_url('noticia')?>" style="color: white">Noticias</a></li>
                  <li class="dropdown">
                     <?php $user_image = array(
                        'class'=> 'img-circle',
                        'src' => site_url('resources/img/users/'.$usuario_conectado['image']),
                        'height' => '25px',
                        'width' => '25px',
                        'alt' => 'userIcon',
                        );  ?>
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: white"><?=img($user_image) ?> <?=$usuario_conectado['email']; $usuario_conectado_id = $usuario_conectado['id']; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu" role="menu">
                        <li><a href="<?=site_url("perfil/muro/$usuario_conectado_id");?>">Perfil</a></li>
                        <li><a href="<?=site_url("configuracion/preferencias");?>">Configuración</a></li>
                        <li><a href="<?=site_url("mensajes");?>">Mensajes</a></li>
                        <?php 
                           if($usuario_conectado['role'] == 'admin') {
                             echo '<li class="divider"></li>';
                             echo '<li><a href="'.site_url("administracion/news").'">Administración</a></li>';
                           }
                           ?>
                        <li class="divider"></li>
                        <li><a href="<?=site_url("user/logout");?>">Salir</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </div>