<?php $this->load->view("inc/cabecera_privada"); ?>
<br><br>
<div class="container">
   <div class="row">
      <form role="form" id="formFiltra" name="formFiltraJuegos" action="<?=site_url('juego/juegos_filtrados') ?>" method="POST">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="input-group">
              <input id="inputTexto" name="inputTexto" type="text" class="form-control">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
              </span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-10 col-md-offset-4">
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckboxPs4" name="plataformas[]" value="PS4"><span class="label label-primary"> PS4</span>
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckboxXone" name="plataformas[]" value="XONE"><span class="label label-success"> XONE</span>
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckboxWiiu" name="plataformas[]" value="Wii U"><span class="label label-info"> WiiU</span>
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckboxPc" name="plataformas[]" value="PC"><span class="label label-default"> PC</span>
            </label>
          </div>
        </div>
      </form>
    </div>
    <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="media-list">
         <?php foreach($games as $game) { ?>
         <li class="media">
          <?php $game_image = array(
            'src' => site_url('resources/img/games/'.$game['imagen']),
            'class' => 'media-object',
            'height' => '65px',
            'alt' => 'userIcon',
            );  ?>
            <div class="media-left">
             <?=anchor('juego/'.$game['id'],img($game_image))?>
           </div>
           <div class="media-body">
            <a href="<?=site_url('juego/'.$game['id'])?>">
             <h4 class="media-heading" id="juego_<?=$game['id']?>" name="juego_<?=$game['id']?>">  
                <?=$game['titulo']?>
             </h4>
           </a>
            <?=substr($game['descripcion'],0,200).'...';?>
           </div>
         </li>
       <?php } ?>
       </ul>
      </div>
    </div>
  </div>
</div>

<br><br>
<?php $this->load->view("inc/pie_privada"); ?>
<script type="text/javascript" language="javascript">

</script>