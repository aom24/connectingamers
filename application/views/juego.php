<?php $this->load->view("inc/cabecera_privada"); 
   ?>
<br>
<div class="container">
   <br>
   <?php 
      $this->load->view("inc/juegos/cabecera_juego"); 
      ?>
   <div class="col-md-7">
      <table class="table table-borderless">
         <tr>
            <td><b>Género: </b><span id="genero" class="label label-danger"><?=$juego->genero ?></span></td>
         </tr>
         <tr>
            <td><b>Plataforma: </b><span id="plataforma" class="label label-warning"><?=$juego->plataforma ?></span></td>
         </tr>
         <tr>
            <td><?=$juego->descripcion ?></td>
         </tr>
         <tr>
            <td><b>Personas que lo han añadido a sus listas:</b></td>
         </tr>
         <tr>
            <td>
               <?php foreach ($usuarios as $usuario_juego) {
                  switch($usuario_juego['user_estado']) {
                    case 'Jugando': $frase = ' lo está '; break;
                    case 'Pendiente': $frase = ' lo tiene '; break;
                    case 'Jugado': $frase = ' lo ha '; break;
                    default: $frase = ' ';
                  }
                  
                  $usuario_juego_imagen = array(
                    'src' => site_url('resources/img/users/'.$usuario_juego['user_image']),
                    'alt' => 'userIcon',
                    'height' => '60px',
                    'width' => '60px',
                    'class' => 'img-thumbnail',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'bottom',
                    'title' => $usuario_juego['user_email'].$frase.$usuario_juego['user_estado'],
                  ); ?>
               <?=anchor('perfil/muro/'.$usuario_juego['id'],img($usuario_juego_imagen)); }?>
               <?php if(empty($usuarios)) {
                  echo '<span class="label label-default">Nadie está interesado en este juego por ahora.</span>';
                  }?>
            </td>
         </tr>
         <tr>
            <td><b>También pueden interesarte: </b></td>
         </tr>
         <tr>
            <td>
               <?php foreach ($listadoJuegos as $game) {
                  $game_image = array(
                    'src' => site_url('resources/img/games/'.$game->imagen),
                    'alt' => 'userIcon',
                    'height' => '120px',
                    'width' => '100px',
                  ); ?>
               <?php echo anchor('juego/'.$game->id,img($game_image));  }?>
               <?php if(empty($listadoJuegos)) {
                  echo '<span class="label label-default">No tenemos más juegos de este mismo género.</span>';
                  }?>
            </td>
         </tr>
      </table>
      <hr>
      <h4> Críticas </h4>
      <div class="alert alert-info" id="errorCritica" style="display: none">
         <button type="button" class="close">&times;</button>
         <strong>Crítica en blanco.</strong> No se pueden enviar críticas sin texto alguno.
      </div>
      <ul class="media-list">
         <?php foreach($criticas as $critica) { ?>
         <li class="media well well-sm">
            <?php $critico_imagen = array(
               'src' => site_url('resources/img/users/'.$critica['image_emisor']),
               'alt' => 'userIcon',
               'height' => '40px',
               'width' => '40px',
               'class' => 'media-object img-circle',
               ); ?>
            <?=anchor('perfil/muro/'.$critica['id_emisor'],img($critico_imagen),'class="pull-left"') ?>       
            <?php if($usuario_conectado['id'] == $critica['id_emisor']) {
               echo anchor('juego/delete_critica/' . $juego->id . '/' . $critica['id_critica'],'<span class="glyphicon glyphicon-remove"></span>','class="pull-right"');
               }?>
            <div class="media-body">
               <h5 class="media-heading"><?=anchor('perfil/muro/'.$critica['id_emisor'],$critica['email_emisor']) ?></h5>
               <p><?=nl2br($critica['message']) ?></p>
               <h6><small><?=$critica['fecha'] ?></small></h6>
            </div>
         </li>
         <?php }?>
      </ul>
      <div class="row">
         <div class="col-md-12">
            <nav>
               <center><?=$this->pagination->create_links()?></center>
            </nav>
         </div>
      </div>
      <form class="form-horizontal" role="form" id="formMuro" name="formMuro" action="<?=site_url('juego/send_critica/' . $juego->id) ?>"  method="POST" onsubmit="return revisarCritica()">
         <div class="form-group">
            <div class="col-md-12">
               <textarea class="form-control" id="inputCritica" name="inputCritica" rows="4" placeholder="Escribe tu crítica... "></textarea>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-offset-10 col-md-1">
               <button type="submit" class="btn btn-default">Enviar</button>
            </div>
         </div>
      </form>
   </div>
   <hr>
</div>
<?php 
   $this->load->view("inc/pie_privada"); 
   ?> 
<?php 
   $this->load->view("inc/juegos/pie_juego"); 
   ?>

<script type="text/javascript">
   <?php 
      if($this->session->flashdata('errorEstado') == true) {
        echo '$("#errorEstado").show(400);';
      }
      if($this->session->flashdata('errorPuntuacion') == true) {
        echo '$("#errorPuntuacion").show(400);';
      }
      ?>
   
   $(function () {
       $('[data-toggle="tooltip"]').tooltip()
     })
   
   function revisarCritica() {
    if(document.getElementById("inputCritica").value.trim() == '') { $('#errorCritica').show(400); return false; }
    else { return true; }
   }
        
   $('.close').click(function() { $('.alert').hide(400)})
</script>