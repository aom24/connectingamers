<?php $this->load->view("inc/cabecera_privada"); ?>
<br>
<div class="container">
   <div class="row">
      <h3><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Mejor puntuados</h3>
      <?php foreach ($listadoJuegos as $juego) {?>
      <div class="col-xs-6 col-md-2">
         <?php $game_image = array(
            'class'=> 'media-object',
            'src' => site_url('resources/img/games/'.$juego->imagen),
            'alt' => 'userIcon',
            );  ?>
         <?=anchor('juego/'.$juego->id,img($game_image))?>
         <h4><?=$juego->titulo ?></h4>
         <h5> <span class="label label-warning"><?=$juego->puntuacion ?></span> 
            <span id="span_plataforma" class="<?php echo 'label '; if ($juego->plataforma == 'PS4') { echo 'label-primary'; } else if ($juego->plataforma == 'XONE') { echo 'label-success'; } else if ($juego->plataforma == 'PC') { echo 'label-default'; } else { echo 'label-info'; }?>"><?=$juego->plataforma ?></span>
            <span class="label label-danger"><?=$juego->genero ?></span> 
         </h5>
      </div>
      <?php }?>
      <form action="juego">
         <button type="submit" class="btn btn-default btn-xs btn-block">Ver más</button><br>
      </form>
   </div>
   <div class="row">
      <h3><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> Últimas noticias</h3>
      <?php foreach ($listadoNoticias as $noticia) { ?>
      <div class="col-sm-12 col-md-6">
         <div class="col-sm-12 col-md-12">
            <div class="thumbnail">
               <div class="caption">
                  <h3 style="text-align: center"><?=$noticia['title'] ?></h3>
                  <?=substr($noticia['description'],0,200).'...'; echo anchor('noticia/'.$noticia['id'],'Leer mas') ?>
                  <small>
                     <p><?=$noticia['date'] ?> <i class="fa fa-comments"> <?=$noticia['num_comments']?></i></p>
                  </small>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
      <form action="noticia">
         <button type="submit" class="btn btn-default btn-xs btn-block">Ver más</button><br>
      </form>
   </div>
   <div class="row">
      <div class="col-xs-12 col-md-3" style="background-color: white">
         <h3 ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> ¿Quien te sigue?</h3>
         <hr>
         <ul class="media-list">
            <?php foreach($listadoSeguidores as $seguidor) {?>
            <?php $seguidor_imagen = array(
               'src' => site_url('resources/img/users/'.$seguidor['image']),
               'alt' => 'userIcon',
               'height' => '45px',
               'width' => '45px',
               'class' => 'media-object img-circle',
               ); ?>
            <li class="media">
               <div class="media-left">
                  <?=anchor('perfil/muro/'.$seguidor['id'],img($seguidor_imagen)) ?>
               </div>
               <div class="media-body">
                  <h4 class="media-heading"><?=anchor('perfil/muro/'.$seguidor['id'],$seguidor['email']) ?></a></h4>
                  <span class="label label-primary"><?=$seguidor['total_seguidores'] ?> Seguidores</span> <span class="label label-danger"><?=$seguidor['total_seguidos'] ?> Siguiendo </span>
               </div>
            </li>
            <?php } ?>
         </ul>
      </div>
      <div class="col-xs-12 col-md-8 col-md-offset-1" style="background-color: white">
         <h3><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Últimas críticas</h3>
         <?php foreach($listadoCriticas as $critica) { ?>
         <?php $critico_imagen = array(
            'src' => site_url('resources/img/users/'.$critica['image_emisor']),
            'alt' => 'userIcon',
            'height' => '25px',
            'width' => '25px',
            'class' => 'media-object img-circle pull-left',
            ); ?>
         <?php $critica_juego = array(
            'src' => site_url('resources/img/games/'.$critica['image_juego']),
            'alt' => 'userIcon',
            'height' => '95px',
            'class' => 'media-object',
            ); ?>
         <hr>
         <div class="media">
            <div class="media-left">
               <?=anchor('juego/'.$critica['id_juego'],img($critica_juego))  ?>
            </div>
            <div class="media-body">
               <h4 class="media-heading"><?=anchor('juego/'.$critica['id_juego'],$critica['titulo_juego']) ?></h4>
               <p><?=nl2br($critica['message']) ?></p>
               <h6><small><?=$critica['fecha'] ?></small></h6>
               <?=anchor('perfil/muro/'.$critica['id_emisor'],img($critico_imagen)); echo '&nbsp&nbsp'; echo anchor('perfil/muro/'.$critica['id_emisor'],$critica['email_emisor']); ?>
            </div>
         </div>
         <?php } ?>
         <br>
      </div>
   </div>
</div>
<br><br>
<?php $this->load->view("inc/pie_privada"); ?>