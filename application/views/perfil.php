<?php 
   $this->load->view("inc/cabecera_privada"); 
   ?>
<br>
<div class="container">
   <div class="row">
      <?php $this->load->view("inc/perfil/cabecera_perfil"); ?>
   </div>
   <div class="row">
      <?php if($sigue || $usuario->id == $usuario_conectado['id']) {
              $this->load->view("inc/perfil/menu_perfil"); 
            } else {
              $this->load->view("inc/perfil/menu_perfil_block");
            } ?>
      <div class="col-md-8">
        <div class="well">
        <?php if(($sigue || $usuario->id == $usuario_conectado['id']) && !$esBloqueador) {
                
                if($menu == "muro") { 
                  $this->load->view("inc/perfil/perfil_muro"); 
                } else if($menu == "juegos") {
                  $this->load->view("inc/perfil/perfil_juegos");
                } else if($menu == "social") {
                  $this->load->view("inc/perfil/perfil_social");
                }
              } else {
                $this->load->view("inc/perfil/perfil_block");
              }
         ?> 
        </div>
      </div>
   </div>
</div>
<!-- container -->
<?php 
   $this->load->view("inc/pie_privada"); 
   ?> 
<script type="text/javascript">
   <?php 
      if($this->session->flashdata('errorDejarSeguir') == true) {
        echo '$("#errorDejarSeguir").show(400);';
      }
      if($this->session->flashdata('errorSiguiendo') == true) {
        echo '$("#errorSiguiendo").show(400);';
      }
      if($this->session->flashdata('errorFollowPagina') == true) {
        echo '$("#errorFollowPagina").show(400);';
      }
      ?>
     
   function revisarMessage() {
    if(document.getElementById("inputMessage").value.trim() == '') { $('#errorMessagePerfil').show(400); return false; }
    else { return true; }
   }

   $('.close').click(function() { $('.alert').hide(400)})
</script>