<?php $this->load->view("inc/cabecera_privada"); ?>
<br><br>
<div class="container">
   <div class="row">
      <div class="list-group">
         <?php $fecha=null; $first=true?>
         <?php foreach($news as $new) { ?>
         <?php
            if($first) { echo '<h3>'.substr($new['date'], 0, 10).'</h3>'; $first=false; }
             else {
               if($fecha > $new['date']) { echo '<h3>'.substr($new['date'], 0, 10).'</h3>'; }
             }
              $fecha = substr($new['date'], 0, 10);
            ?>
         <a href="<?=site_url('noticia/'.$new['id'])?>" class="list-group-item" id="noticia_<?=$new['id']?>" name="noticia_<?=$new['id']?>">
            <h4 class="list-group-item-heading"><?=$new['title']?></h4>
            <p class="list-group-item-text"><?=substr($new['description'],0,200).'...';?></p>
            <h6><small><?=$new['date']?></small> <i class="fa fa-comments"> <?=$new['num_comments']?></i></h6>
         </a>
         <?php } ?>
      </div>
   </div>
</div>
<br><br>
<?php $this->load->view("inc/pie_privada"); ?>
<script type="text/javascript" language="javascript">
   $(document).ready(function() {
           $("a[name^='noticia_']").hover(
             function() {
               $(this).attr("class", "list-group-item active");
           }, function() {
                 $( this ).attr("class", "list-group-item");
               }
           );
   })
</script>