$(document).ready(function()
{
    $.fn.delayPasteKeyUp = function(fn, ms)
    {
        var timer = 0;
        $(this).on("keyup paste", function()
        {
            clearTimeout(timer);
            timer = setTimeout(fn, ms);
        });
    };
 
    $("input[name=autocomplete]").delayPasteKeyUp(function()
    {   
        $.post(urlbusca,{email : $("input[name=autocomplete]").val()}, function(data) { 
            if(data)
            {   
                $("#busqueda").html(data);
            }
        })
    }, 5);

    $(document).ready(function() {
           $("a[name^='usuario_']").hover(
             function() {
               $(this).attr("class", "list-group-item active");
           }, function() {
                 $( this ).attr("class", "list-group-item");
               }
           );
   })
});